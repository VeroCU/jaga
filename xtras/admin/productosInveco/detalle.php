<?php
$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
$row_catalogo = $consulta -> fetch_assoc();


$fechaSQL=$row_catalogo['fecha'];
$segundos=strtotime($fechaSQL);
$fechaUI=date('m/d/Y',$segundos);


echo '
	<div class="uk-width-1-1 margin-v-20">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'" class="color-red">'.$row_catalogo['titulo'].'</a></li>
		</ul>
	</div>
	<div class="uk-width-1-2@s margin-v-20">
		<div class="uk-card uk-card-default uk-card-body">
			<div>
				<span class=" uk-text-muted">SKU:</span>
				'.$row_catalogo['sku'].'
			</div>
			<div>
				<span class="uk-text-capitalize uk-text-muted">titulo:</span>
				'.$row_catalogo['titulo'].'
			</div>
			<div>
				<span class="uk-text-capitalize uk-text-muted">precio:</span>
				'.number_format($row_catalogo['precio'],2).'
			</div>
			<div>
				<span class="uk-text-capitalize uk-text-muted">descuento:</span>
				'.$row_catalogo['descuento'].'%
			</div>
			<div>
				<span class="uk-text-capitalize uk-text-muted">Descripción:</span>
				'.$row_catalogo['txt'].'
			</div>
			<div class="uk-width-1-1 uk-text-right">
				<span class="uk-text-muted">Fecha de captura:</span>
				'.$fechaUI.'
			</div>
		</div>
	</div>
	<div class="uk-width-1-2@s margin-v-20">
		<div class="uk-width-1-1 uk-margin-top">
			<div class="uk-card uk-card-default uk-card-body">
				<div class="uk-width-1-1 uk-margin-top">
					<h4>SEO</h4>
					<span class="uk-text-capitalize uk-text-muted">titulo google:</span>
					'.$row_catalogo['title'].'
				</div>
				<div class="uk-width-1-1">
					<span class="uk-text-capitalize uk-text-muted">descripción google:</span>
					'.$row_catalogo['metadescription'].'
				</div>

				<div class="uk-width-1-1">
					<div class="margin-top-50">
						<h3>Ficha técnica</h3>
						<p class="uk-text-muted">Archivos tipo: PDF</p>
					</div>
					<div class="uk-width-1-1">
						<div id="fileuploaderpdf">
							Cargar
						</div>
					</div>
					<div class="uk-width-1-1 uk-text-center margin-v-20">';
	// Ficha técnica
	$pdfFile='../img/contenido/'.$seccion.'pdf/'.$row_catalogo['pdf'];
	if(strlen($row_catalogo['pdf'])>0 AND file_exists($pdfFile)){
		echo '
						<div class="uk-panel uk-text-center">
							<a class="uk-button uk-button-primary uk-button-large " href="'.$pdfFile.'" target="_blank">
								<span uk-icon="download"></span>
								Descargar PDF
							</a><br><br>
							<button class="uk-button uk-button-danger uk-button-large borrarpdf"><i uk-icon="icon:trash"></i> Eliminar pdf</button>
						</div>';
	}else{
		echo '
						<div class="uk-panel">
							<p class="uk-scrollable-box"><i uk-icon="icon:warning;ratio:4;"></i><br><br>
								Faltan ficha técnica<br><br>
							</p>
						</div>';
	}
	echo '
					</div>
				</div>
			</div>
		</div>
	</div>



	<div class="uk-width-1-1 margin-v-20">
		<div class="uk-card uk-card-default uk-card-body">
			<h5>Categorias</h5>
			<div class="uk-child-width-1-5@m uk-child-width-1-2 uk-grid-small" uk-grid>';

			// Obtener Categorias
			$CONSULTA = $CONEXION -> query("SELECT * FROM productoscat ORDER BY orden");
			while ($rowCONSULTA = $CONSULTA -> fetch_assoc()) {
				$catID=$rowCONSULTA['id'];
				$CONSULTAX = $CONEXION -> query("SELECT * FROM productoscatrel WHERE item = $id AND valor = $catID");
				$numRows=$CONSULTAX->num_rows;
				$estatusIcon=($numRows==0)?'off uk-text-muted':'on uk-text-primary';

				echo '
				<div> 
					<i class="relajax fa fa-lg fa-toggle-'.$estatusIcon.' uk-text-muted pointer" data-tabla="productoscatrel" data-id="'.$id.'" data-valor="'.$catID.'" data-estatus="'.$numRows.'"></i> '.$rowCONSULTA['txt'].'
				</div>';
			}


		echo '
			</div>
		</div>
	</div>

	<div class="uk-width-1-1 margin-v-20">
		<div class="uk-card uk-card-default uk-card-body">
			<h5>Categorias</h5>
			<div class="uk-child-width-1-5@m uk-child-width-1-2 uk-grid-small" uk-grid>';
			
			// Obtener Subcategorias
			$CONSULTA = $CONEXION -> query("SELECT * FROM productossub ORDER BY orden");
			while ($rowCONSULTA = $CONSULTA -> fetch_assoc()) {
				$catID=$rowCONSULTA['id'];
				$CONSULTAX = $CONEXION -> query("SELECT * FROM productossubrel WHERE item = $id AND valor = $catID");
				$numRows=$CONSULTAX->num_rows;
				$estatusIcon=($numRows==0)?'off uk-text-muted':'on uk-text-primary';

				echo '
				<div> 
					<i class="relajax fa fa-lg fa-toggle-'.$estatusIcon.' uk-text-muted pointer" data-tabla="productossubrel" data-id="'.$id.'" data-valor="'.$catID.'" data-estatus="'.$numRows.'"></i> '.$rowCONSULTA['titulo'].'
				</div>';
			}


		echo '
			</div>
		</div>
	</div>';




// Imagen principal
	echo '
	<div class="uk-width-1-1 margin-v-50">
		<h3 class="uk-text-center">Imagen principal</h3>
	</div>
	<div class="uk-width-1-2@s margin-top-50">
		<div class="margin-bottom-50 uk-text-muted">
			Archivo JPG<br><br>
			600 px de ancho<br>
			600 px de alto
		</div>
		<div id="fileuploadermain">
			Cargar
		</div>
	</div>
	<div class="uk-width-1-2@s uk-text-center margin-v-20">';

		$pic='../img/contenido/'.$seccion.'main/'.$row_catalogo['imagen'];
		if(strlen($row_catalogo['imagen'])>0 AND file_exists($pic)){
			echo '
			<div class="uk-panel uk-text-center">
				<a href="'.$pic.'" target="_blank">
					<img src="'.$pic.'" class=" uk-border-rounded margin-top-20">
				</a><br><br>
				<button class="uk-button uk-button-danger borrarpic"><i uk-icon="icon:trash"></i> Eliminar imagen</button>
			</div>';
		}elseif(strlen($row_catalogo['imagen'])>0 AND strpos($row_catalogo['imagen'], 'ttp')>0){
			echo '
			<div class="uk-panel uk-text-center">
				<div class="uk-width-1-1">
					<a href="'.$row_catalogo['imagen'].'" target="_blank">
						<img src="'.$row_catalogo['imagen'].'" class=" uk-border-rounded margin-top-20">
					</a>
				</div>
				<div class="uk-width-1-1">
					Link a la imagen:
				</div>
				<div class="uk-width-1-1">
					<input class="editarajax uk-input" data-tabla="productos" data-campo="imagen" data-id="'.$id.'" value="'.$row_catalogo['imagen'].'" tabindex="10" placeholder="Ejemplo: https://image.ibb.co/e2eXT7/Fotolia-141780386-Subscription-Monthly-M.jpg">
				</div>
			</div>';
		}elseif(strlen($row_catalogo['imagen'])>0 AND strpos($row_catalogo['imagen'], 'contenido')>0){
			echo '
			<div class="uk-panel uk-text-center">
				<div class="uk-width-1-1">
					<a href="../'.$row_catalogo['imagen'].'" target="_blank">
						<img src="../'.$row_catalogo['imagen'].'" class=" uk-border-rounded margin-top-20">
					</a>
				</div>
				<div class="uk-width-1-1">
					Link a la imagen:
				</div>
				<div class="uk-width-1-1">
					<input class="editarajax uk-input" data-tabla="productos" data-campo="imagen" data-id="'.$id.'" value="'.$row_catalogo['imagen'].'" tabindex="10" placeholder="Ejemplo: https://image.ibb.co/e2eXT7/Fotolia-141780386-Subscription-Monthly-M.jpg">
				</div>
			</div>';
		}else{
			echo '
			<div class="uk-panel uk-text-center">
				<div class="uk-width-1-1">
					<p class="uk-scrollable-box"><i uk-icon="icon:image;ratio:5;"></i><br><br>
						<br><br>
					</p>
				</div>
				<div class="uk-width-1-1">
					Link a la imagen:
				</div>
				<div class="uk-width-1-1">
					<input class="editarajax uk-input" data-tabla="productos" data-campo="imagen" data-id="'.$id.'" value="'.$row_catalogo['imagen'].'" tabindex="10" placeholder="Ejemplo: https://image.ibb.co/e2eXT7/Fotolia-141780386-Subscription-Monthly-M.jpg">
				</div>
			</div>';
		}
	echo '
	</div>';



echo '
	<!-- Fotografías -->
	<div class="uk-width-1-1 margin-top-50">
		<h3 class="uk-text-center">Fotografías</h3>
	</div>

	<div class="uk-width-1-1">
		<div id="fileuploader">
			Cargar
		</div>
	</div>
	<div class="uk-width-1-1 uk-text-center">
		<div uk-grid id="pics" class="uk-grid-small uk-grid-match sortable" data-tabla="'.$seccionpic.'">';
	$rutaFinal = '../img/contenido/productos/';
	$consultaPIC = $CONEXION -> query("SELECT * FROM $seccionpic WHERE producto = $id ORDER BY orden,id");
	$numProds=$consultaPIC->num_rows;
	while ($row_consultaPIC = $consultaPIC -> fetch_assoc()) {
		$picId=$row_consultaPIC['id'];
		$pic=$rutaFinal.$picId.'.jpg';
		if(file_exists($pic)){
			echo '
				<div class="uk-width-1-4@l uk-width-1-2@m uk-width-1-1@s uk-margin-bottom" id="'.$picId.'">
					<div class="uk-card uk-card-default uk-card-body uk-text-center">
						<a href="javascript:borrarfoto(\''.$picId.'\')" class="uk-icon-button uk-button-danger" uk-icon="trash"></a>
						<div class="uk-margin" uk-lightbox>
							<a href="'.$pic.'">
								<img src="'.$pic.'" class="uk-border-rounded margin-top-20">
							</a>
						</div>
					</div>
				</div>';
		}else{
			echo '
				<div class="uk-width-1-4@l uk-width-1-2@m uk-width-1-1@s uk-margin-bottom" id="'.$picId.'">
					<div class="uk-card uk-card-default uk-card-body uk-text-center">
						<a href="javascript:borrarfoto(\''.$picId.'\')" class="uk-icon-button uk-button-danger" uk-icon="trash"></a>
						<br>
						Imagen rota<br>
						<i uk-icon="icon:ban;ratio:2;"></i><br>
						'.$pic.'
					</div>
				</div>';
		}
	}
	echo '
		</div>
	</div>
	';


// Buttons
	echo '
	<div style="min-height:300px;">
	</div>

	<div>
		<div id="buttons">
			<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=editar&id='.$id.'" class="uk-icon-button uk-button-primary uk-box-shadow-large" uk-icon="icon:pencil;ratio:1.4;"></a> &nbsp;&nbsp;
			<button data-id="'.$row_catalogo['id'].'" class="eliminaprod uk-icon-button uk-button-danger uk-box-shadow-large" uk-icon="icon:trash;ratio:1.4;"></button> &nbsp;&nbsp;
			<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=nuevo&cat='.$cat.'" class="uk-icon-button uk-button-primary uk-box-shadow-large" uk-icon="icon:plus;ratio:1.4;"></a> &nbsp;&nbsp;
			<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
		</div>
	</div>
	';



$scripts='


	// Eliminar foto
	function borrarfoto (id) { 
		var statusConfirm = confirm("Realmente desea eliminar esto?"); 
		if (statusConfirm == true) { 
			$.ajax({
				method: "POST",
				url: "modulos/'.$seccion.'/acciones.php",
				data: { 
					borrarfoto: 1,
					id: id
				}
			})
			.done(function( msg ) {
				UIkit.notification.closeAll();
				UIkit.notification(msg);
				$("#"+id).addClass( "uk-invisible" );
			});
		}
	}

	$(document).ready(function() {
		$("#fileuploader").uploadFile({
			url:"modulos/'.$seccion.'/acciones.php?id='.$id.'",
			multiple: true,
			maxFileCount:1000,
			fileName:"uploadedfile",
			allowedTypes: "jpeg,jpg",
			maxFileSize: 6000000,
			showFileCounter: false,
			showDelete: "false",
			showPreview:false,
			showQueueDiv:true,
			returnType:"json",
			onSuccess:function(files,data,xhr){
				console.log(data);
				var l = data[0].indexOf(".");
				var id = data[0].substring(0,l);
				$("#pics").append("';
				$scripts.='<div class=\'uk-width-1-4@l uk-width-1-2@m uk-width-1-1@s uk-margin-bottom\' id=\'"+id+"\'>';
				$scripts.='<div class=\'uk-card uk-card-default uk-card-body uk-text-center\'>';
				$scripts.='<div>';
				$scripts.='<a href=\'javascript:borrarfoto(\""+id+"\")\' class=\'uk-icon-button uk-button-danger\' uk-icon=\'trash\'></a>';
				$scripts.='</div>';
				$scripts.='<div class=\'uk-margin\' uk-lightbox>';
				$scripts.='<a href=\''.$rutaFinal.'"+data+"\'>';
				$scripts.='<img src=\''.$rutaFinal.'"+data+"\'>';
				$scripts.='</a>';
				$scripts.='</div>';
				$scripts.='</div>';
				$scripts.='</div>';
				$scripts.='");';
				$scripts.='
			}
		});
	});



	$(document).ready(function() {
		$("#fileuploadermain").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "jpeg,jpg",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&cat='.$cat.'&id='.$id.'&position=main&imagen=\'+data);
			}
		});

		$("#fileuploaderpdf").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "pdf",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&cat='.$cat.'&id='.$id.'&position=pdf&imagen=\'+data);
			}
		});
	});

	// Eliminar producto
	$(".eliminaprod").click(function() {
		var id = $(this).attr(\'data-id\');
		//console.log(id);
		var statusConfirm = confirm("Realmente desea eliminar este Producto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=catdetalle&borrarPod&cat='.$cat.'&id="+id);
		} 
	});

	// Borrar foto redes
	$(".borrarpic").click(function() {
		var statusConfirm = confirm("Realmente desea borrar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&borrarpicredes");
		} 
	});

	// Borrar PDF
	$(".borrarpdf").click(function() {
		var statusConfirm = confirm("Realmente desea borrar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&borrarpdf");
		} 
	});

	';
