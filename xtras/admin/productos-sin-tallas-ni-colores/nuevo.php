<?php

echo '
<div class="uk-width-1-1 margen-v-20 uk-text-left">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?seccion='.$seccion.'">Productos</a></li>
		<li><a href="index.php?seccion='.$seccion.'&subseccion=nuevo" class="color-red">Nuevo</a></li>
	</ul>
</div>


<form action="index.php" class="uk-width-1-1" method="post" name="editar" onsubmit="return checkForm(this);">
	<input type="hidden" name="nuevo" value="1">
	<input type="hidden" name="seccion" value="'.$seccion.'">

	<div uk-grid class="uk-grid-small uk-child-width-1-3@m">
		<div>
			<label class="uk-text-capitalize" for="sku">SKU</label>
			<input type="text" class="uk-input" name="sku" autofocus required>
		</div>
		<div>
			<label class="uk-text-capitalize" for="titulo">título</label>
			<input type="text" class="uk-input" name="titulo" id="titulo" placeholder="" required>
		</div>
		<div>
			<label class="uk-text-capitalize" for="precio">Precio</label>
			<input type="text" class="uk-input" name="precio" id="precio" placeholder="">
		</div>



		<div class="uk-width-1-1">
			<label class="uk-text-capitalize" for="txt">descripción</label>
			<textarea class="editor" name="txt" id="txt"></textarea>
		</div>
		<div class="uk-width-1-1">
			<p>SEO</p>
		</div>
		<div class="uk-width-1-1">
			<label class="uk-text-capitalize" for="title">titulo google</label>
			<input type="text" class="uk-input" name="title" id="title" placeholder="Término como alguien nos buscaría">
		</div>
		<div class="uk-width-1-1">
			<label class="uk-text-capitalize" for="metadescription">descripción google</label>
			<textarea class="uk-textarea" name="metadescription" id="metadescription" placeholder="Descripción explícita para que google muestre a quienes nos vean en las búsquedas"></textarea>
		</div>
		<div class="uk-width-1-1 uk-text-center">
			<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
		</div>
	</div>
</form>

<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>';



$scripts='
'; 
