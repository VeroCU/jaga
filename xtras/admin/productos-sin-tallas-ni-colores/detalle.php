<?php 
	$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
	$rowCONSULTA = $consulta -> fetch_assoc();
	$link='../'.$id.'_'.urlencode(str_replace($caracteres_no_validos,$caracteres_si_validos,html_entity_decode(strtolower($rowCONSULTA['titulo'])))).'_.html';

	$fechaSQL=$rowCONSULTA['fecha'];
	$segundos=strtotime($fechaSQL);
	$fechaUI=date('m/d/Y',$segundos);

// GALERÍA
	$picTXT='';
	$consultaPIC = $CONEXION -> query("SELECT * FROM $seccionpic WHERE producto = $id ORDER BY orden,id");
	$numProds=$consultaPIC->num_rows;
	while ($row_consultaPIC = $consultaPIC -> fetch_assoc()) {

		$pic='../img/contenido/'.$seccion.'/'.$row_consultaPIC['id'].'-sm.jpg';
		$picLg='../img/contenido/'.$seccion.'/'.$row_consultaPIC['id'].'.jpg';
		if(file_exists($pic)){

			$picTXT.='
					<div class="uk-width-1-4@l uk-width-1-2@m uk-width-1-1@s uk-margin-bottom" id="'.$row_consultaPIC['id'].'">
						<div class="uk-card uk-card-default uk-card-body uk-text-center">
							<a href="'.$picLg.'" class="uk-icon-button uk-button-default" target="_blank" uk-icon="icon:image"></a> &nbsp;
							<a href="javascript:eliminaPic(picID='.$row_consultaPIC['id'].')" class="uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></a>
							<br>
							<img src="'.$pic.'" class="img-responsive uk-border-rounded margen-top-20"><br>
						</div>
					</div>';
		}else{
			$picTXT.='
					<div class="uk-width-1-4@l uk-width-1-2@m uk-width-1-1@s uk-margin-bottom" id="'.$row_consultaPIC['id'].'">
						<div class="uk-card uk-card-default uk-card-body uk-text-center">
							<a href="javascript:eliminaPic(picID='.$row_consultaPIC['id'].')" class="uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></a>
							<br>
							Imagen rota<br>
							<i uk-icon="icon:ban;ratio:2;"></i>
						</div>
					</div>';
		}
	}

echo '
<div class="uk-width-1-1 margen-v-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'" class="color-red">'.$rowCONSULTA['titulo'].'</a></li>
	</ul>
</div>

<div class="uk-width-1-1 uk-text-right margen-v-20">
	<a href="'.$link.'" target="_blank" class="uk-button uk-button-default"><i class="fa fa-lg fa-eye"></i> &nbsp; Ver online</a>
	<a href="index.php?seccion='.$seccion.'&subseccion=nuevo" class="uk-button uk-button-default"><i class="fa fa-lg fa-plus"></i> &nbsp; Nuevo producto</a>
	<a href="index.php?seccion='.$seccion.'&subseccion=editar&id='.$id.'" class="uk-button uk-button-primary"><i class="fas fa-pencil-alt"></i> &nbsp; Editar</a>
	<button data-id="'.$rowCONSULTA['id'].'" class="eliminaprod uk-button uk-button-danger" tabindex="1"><i class="fa fa-lg fa-trash"></i> &nbsp; Eliminar producto</button> 
</div>



<div class="uk-width-1-2 margen-v-20">
	<div class="uk-card uk-card-default uk-card-body">
		<div>
			<span class="uk-text-muted">SKU:</span>
			'.$rowCONSULTA['sku'].'
		</div>
		<div>
			<span class="uk-text-capitalize uk-text-muted">titulo:</span>
			'.$rowCONSULTA['titulo'].'
		</div>
		<div>
			<span class="uk-text-capitalize uk-text-muted">existencias:</span>
			'.number_format($rowCONSULTA['existencias']).' pza
		</div>
		<div>
			<span class="uk-text-capitalize uk-text-muted">precio:</span>
			$'.number_format($rowCONSULTA['precio'],2).'
		</div>
		<div class="uk-margin">
			<span class="uk-text-capitalize uk-text-muted">Descripción:</span>
			'.$rowCONSULTA['txt'].'
		</div>
		<div class="uk-width-1-1 uk-text-right">
			<span class="uk-text-muted">Fecha de captura:</span>
			'.$fechaUI.'
		</div>
	</div>
</div>
<div class="uk-width-1-2 margen-v-20">
	<div class="uk-width-1-1 uk-margin-top">
		<div class="uk-card uk-card-default uk-card-body">
			<div class="uk-width-1-1 uk-margin-top">
				<h4>SEO</h4>
				<span class="uk-text-capitalize uk-text-muted">titulo google:</span>
				'.$rowCONSULTA['title'].'
			</div>
			<div class="uk-width-1-1">
				<span class="uk-text-capitalize uk-text-muted">descripción google:</span>
				'.$rowCONSULTA['metadescription'].'
			</div>
		</div>
	</div>
</div>




';



// Imagen principal
	echo '
	<div class="uk-width-1-2@s margen-top-50">
		<h3 class="uk-text-center">Imagen principal</h3>
		<div class="margen-bottom-50 uk-text-muted">
			Archivo JPG<br><br>
			600 px de ancho<br>
			600 px de alto
		</div>
		<div id="fileuploadermain">
			Cargar
		</div>
	</div>
	<div class="uk-width-1-2@s uk-text-center margen-v-20">'.$fileName;

		$pic='../img/contenido/'.$seccion.'main/'.$rowCONSULTA['imagen'];
		if(strlen($rowCONSULTA['imagen'])>0 AND file_exists($pic)){
			echo '
			<div class="uk-panel uk-text-center">
				<a href="'.$pic.'" target="_blank">
					<img src="'.$pic.'" class=" uk-border-rounded margen-top-20">
				</a><br><br>
				<button class="uk-button uk-button-danger borrarpic"><i uk-icon="icon:trash"></i> Eliminar imagen</button>
			</div>';
		}elseif(strlen($rowCONSULTA['imagen'])>0 AND strpos($rowCONSULTA['imagen'], 'ttp')>0){
			echo '
			<div class="uk-panel uk-text-center">
				<div class="uk-width-1-1">
					<a href="'.$rowCONSULTA['imagen'].'" target="_blank">
						<img src="'.$rowCONSULTA['imagen'].'" class=" uk-border-rounded margen-top-20" style="max-height:400px">
					</a>
				</div>
				<div class="uk-width-1-1">
					Link a la imagen:
				</div>
				<div class="uk-width-1-1">
					<input class="editarajax uk-input" data-tabla="productos" data-campo="imagen" data-id="'.$id.'" value="'.$rowCONSULTA['imagen'].'" tabindex="10" placeholder="Ejemplo: https://image.ibb.co/e2eXT7/Fotolia-141780386-Subscription-Monthly-M.jpg">
				</div>
			</div>';
		}else{
			echo '
			<div class="uk-panel uk-text-center">
				<div class="uk-width-1-1">
					<p class="uk-scrollable-box"><i uk-icon="icon:image;ratio:5;"></i><br><br>
						<br><br>
					</p>
				</div>
				<div class="uk-width-1-1">
					Link a la imagen:
				</div>
				<div class="uk-width-1-1">
					<input class="editarajax uk-input" data-tabla="productos" data-campo="imagen" data-id="'.$id.'" value="'.$rowCONSULTA['imagen'].'" tabindex="10" placeholder="Ejemplo: https://image.ibb.co/e2eXT7/Fotolia-141780386-Subscription-Monthly-M.jpg">
				</div>
			</div>';
		}
	echo '
	</div>';



echo '
<!-- Galería -->
	<div class="uk-width-1-1">
		<h3 class="uk-text-center">Galería</h3>
	</div>

	<div class="uk-width-1-1">
		<div id="fileuploader">
			Cargar
		</div>
	</div>
	<div class="uk-width-1-1 uk-text-center">
		<div uk-grid class="uk-grid-small uk-grid-match sortable" data-tabla="'.$seccionpic.'">
			'.$picTXT.'
		</div>
	</div>





<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>


';


$scripts='
	$(document).ready(function() {
		$("#fileuploader").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "jpeg,jpg",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&position=gallery&imagen=\'+data);
			}
		});

		$("#fileuploadermain").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "jpeg,jpg",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&position=main&imagen=\'+data);
			}
		});
	});


	// Eliminar producto
	$(".eliminaprod").click(function() {
		var id = $(this).attr(\'data-id\');
		//console.log(id);
		var statusConfirm = confirm("Realmente desea eliminar este Producto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?seccion='.$seccion.'&subseccion=contenido&borrarPod&id="+id);
		} 
	});

	// Borrar foto
	function eliminaPic () { 
		var statusConfirm = confirm("Realmente desea eliminar esta foto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&borrarPic&picID="+picID);
		} 
	};

	// Borrar foto redes
	$(".borrarpic").click(function() {
		var statusConfirm = confirm("Realmente desea borrar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&borrarpicredes");
		} 
	});


	$(".star").click(function(){
		var valor = $(this).data("valor");
		$.ajax({
			method: "POST",
			url: "modulos/varios/acciones.php",
			data: { 
				editarajax: 1,
				tabla: "productos",
				campo: "calificacion",
				id: '.$id.',
				valor: valor
			}
		})
		.done(function( msg ) {
			window.location = ("index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'");
		});		
	})

	';
