<?php 
$pag=(isset($_GET['pag']))?$_GET['pag']:0;
$prodspagina=(isset($_GET['prodspagina']))?$_GET['prodspagina']:20;
$consulta = $CONEXION -> query("SELECT * FROM $seccion");

$numItems=$consulta->num_rows;
$prodInicial=$pag*$prodspagina;

echo '
<div class="uk-width-1-3 margen-top-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'" class="color-red">Productos &nbsp; <span class="uk-text-muted uk-text-lowercase"> &nbsp; <b>'.$numItems.'</b> productos</span></a></li>
	</ul>
</div>


<div id="acciones" class="uk-width-2-3 uk-text-right margen-v-20">
	<div id="acciones" class="uk-text-right">
		<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=config" class="uk-button uk-button-white"><i uk-icon="icon:cog"></i></a>
		
		<!-- Categorías -->
			<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=categorias" class="uk-button uk-button-primary">Categorías</a>
			<div uk-dropdown>
				<ul class="uk-nav uk-dropdown-nav uk-text-left">';
	// Obtener Categorías
	$CONSULTA = $CONEXION -> query("SELECT * FROM productoscat WHERE parent = 0 ORDER BY orden");
	while ($rowCONSULTA = $CONSULTA -> fetch_assoc()) {
		echo '
					<li><a href="index.php?rand='.rand(1,999).'&seccion='.$seccion.'&subseccion=catdetalle&cat='.$rowCONSULTA['id'].'">'.$rowCONSULTA['txt'].'</a></li>';
	}
	echo '
				</ul>
			</div>

		<!-- Clasificación -->
		<!--
			<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=config&activo=3" class="uk-button uk-button-primary">Clasificación</a>
			<div uk-dropdown>
				<ul class="uk-nav uk-dropdown-nav uk-text-left">';
	// Obtener Clasificaciones
	$CONSULTA = $CONEXION -> query("SELECT * FROM productosclasif ORDER BY orden");
	while ($rowCONSULTA = $CONSULTA -> fetch_assoc()) {
		echo '
					<li><a href="index.php?rand='.rand(1,999).'&seccion='.$seccion.'&subseccion=clasif&id='.$rowCONSULTA['id'].'">'.$rowCONSULTA['txt'].'</a></li>';
	}
	echo '
				</ul>
			</div>
		-->

		<!-- Marcas -->
			<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=config&activo=2" class="uk-button uk-button-primary">Marcas</a>
			<div uk-dropdown>
				<ul class="uk-nav uk-dropdown-nav uk-text-left">';
	// Obtener Marcas
	$CONSULTA = $CONEXION -> query("SELECT * FROM productosmarcas ORDER BY orden");
	while ($rowCONSULTA = $CONSULTA -> fetch_assoc()) {
		echo '
					<li><a href="index.php?rand='.rand(1,999).'&seccion='.$seccion.'&subseccion=marcas&id='.$rowCONSULTA['id'].'">'.$rowCONSULTA['txt'].'</a></li>';
	}
	echo '
				</ul>
			</div>

	</div>
</div>';
?>

<div class="uk-width-1-1">
	<div uk-grid class="uk-grid-small uk-child-width-expand@m uk-child-width-1-2">
		<div><label class="pointer"><i uk-icon="search"></i> SKU<br><input type="text" class="uk-input search" data-campo="sku"></label></div>
		<div><label class="pointer"><i uk-icon="search"></i> Modelo<br><input type="text" class="uk-input search" data-campo="titulo"></label></div>
		<div><label class="pointer"><i uk-icon="search"></i> Material<br><input type="text" class="uk-input search" data-campo="material"></label></div>
		<div><label class="pointer"><i uk-icon="search"></i> Precio<br><input type="text" class="uk-input search" data-campo="precio"></label></div>
		<div><label class="pointer"><i uk-icon="search"></i> Descuento<br><input type="text" class="uk-input search" data-campo="descuento"></label></div>
	</div>
</div>


<div class="uk-width-1-1 margen-v-50">
	<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-table-responsive" id="ordenar">
		<thead>
			<tr class="uk-text-muted">
				<th width="40px"></th>
				<th width="100px" onclick="sortTable(1)" class="pointer">SKU</th>
				<th width="auto"  onclick="sortTable(2)" class="pointer">Modelo</th>
				<th width="100px" onclick="sortTable(3)" class="pointer">Precio</th>
				<th width="90px"  onclick="sortTable(4)" class="pointer">Descuento</th>
				<th width="50px"  onclick="sortTable(5)" class="pointer">En inicio</th>
				<th width="50px"  onclick="sortTable(6)" class="pointer">Activo</th>
				<th width="90px"></th>
			</tr>
		</thead>
		<tbody>
		<?php
		$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion ORDER BY categoria LIMIT $prodInicial,$prodspagina");
		while ($rowCONSULTA = $CONSULTA -> fetch_assoc()) {
			$prodID=$rowCONSULTA['id'];
			$catId=$rowCONSULTA['categoria'];
			$clasifId=$rowCONSULTA['clasif'];

			$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$rowCONSULTA['id'];

			$clasePrecio='';
			$claseDescuento='';
			// Si el precio es 0 pintamos gris
			if ($rowCONSULTA['precio']==0) {
				$clasePrecio='bg-grey';
				$claseDescuento='bg-grey';
			}

			$inicioIcon=($rowCONSULTA['inicio']==0)?'off uk-text-muted':'on uk-text-primary';
			$estatusIcon=($rowCONSULTA['estatus']==0)?'off uk-text-muted':'on uk-text-primary';

			$CONSULTA1 = $CONEXION -> query("SELECT * FROM $seccionpic WHERE producto = $prodID ORDER BY orden");
			$rowCONSULTA1 = $CONSULTA1 -> fetch_assoc();
			$picId=$rowCONSULTA1['id'];
			$picROW='';
			$pic=$rutaFinal.$picId.'-sm.jpg';
			if(file_exists($pic)){
				$picROW='
					<div class="uk-inline">
						<i uk-icon="camera"></i>
						<div uk-drop="pos: right-justify">
							<img uk-img data-src="'.$pic.'" class="uk-border-rounded">
						</div>
					</div>';
			}


			$CONSULTA1 = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $catId");
			$rowCONSULTA1 = $CONSULTA1 -> fetch_assoc();
			$categoriaTxt=$rowCONSULTA1['txt'];
			$parent=$rowCONSULTA1['parent'];

			$CONSULTA1 = $CONEXION -> query("SELECT * FROM productosclasif WHERE id = $clasifId");
			$numClasif = $CONSULTA1->num_rows;
			$clasifTxt = '';
			if ($numClasif>0) {
				$rowCONSULTA1 = $CONSULTA1 -> fetch_assoc();
				$clasifTxt=$rowCONSULTA1['txt'];
			}
			



			echo '
			<tr>
				<td class="uk-text-center">
					'.$picROW.'
				</td>
				<td>
					<input value="'.$rowCONSULTA['sku'].'" type="text" class="editarajax uk-input uk-form-small uk-form-blank" data-tabla="'.$seccion.'" data-campo="sku" data-id="'.$prodID.'" tabindex="6">
				</td>
				<td>
					<input value="'.$rowCONSULTA['titulo'].'" type="text" class="editarajax uk-input uk-form-small uk-form-blank" data-tabla="'.$seccion.'" data-campo="titulo" data-id="'.$prodID.'" tabindex="7">
				</td>
				<td>
					<span class="uk-hidden">'.(10000+(1*($rowCONSULTA['precio']))).'</span>
					<input type="number" class="editarajax uk-input uk-form-small uk-text-right '.$clasePrecio.'" data-tabla="'.$seccion.'" data-campo="precio" data-id="'.$prodID.'" value="'.$rowCONSULTA['precio'].'" tabindex="8">
				</td>
				<td>
					<span class="uk-hidden">'.(10000+(1*($rowCONSULTA['descuento']))).'</span>
					<input type="number" class="editarajax uk-input uk-form-small uk-text-right '.$claseDescuento.'" data-tabla="'.$seccion.'" data-campo="descuento" data-id="'.$prodID.'" value="'.$rowCONSULTA['descuento'].'" tabindex="9">
				</td>
				<td class="uk-text-center">
					<i class="estatuschange pointer fas fa-lg fa-toggle-'.$inicioIcon.'" data-tabla="'.$seccion.'" data-campo="inicio" data-id="'.$rowCONSULTA['id'].'" data-valor="'.$rowCONSULTA['inicio'].'"></i> &nbsp;&nbsp;
				</td>
				<td class="uk-text-center">
					<i class="estatuschange pointer fas fa-lg fa-toggle-'.$estatusIcon.'" data-tabla="'.$seccion.'" data-campo="estatus" data-id="'.$rowCONSULTA['id'].'" data-valor="'.$rowCONSULTA['estatus'].'"></i> &nbsp;&nbsp;
				</td>
				<td class="uk-text-center">
					<a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fa fa-search-plus"></i></a> &nbsp;
					<span data-id="'.$rowCONSULTA['id'].'" class="eliminaprod uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></span>
				</td>
			</tr>';
		}
		?>

		</tbody>
	</table>
</div>




<!-- PAGINATION -->
<div class="uk-width-1-1 padding-top-50">
	<div uk-grid class="uk-flex-center">
		<div>
			<ul class="uk-pagination uk-flex-center uk-text-center">
			<?php
			if ($pag!=0) {
				$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&pag='.($pag-1).'&prodspagina='.$prodspagina;
				echo'
				<li><a href="'.$link.'"><i class="fa fa-lg fa-angle-left"></i> &nbsp;&nbsp; Anterior</a></li>';
			}
			$pagTotal=intval($numItems/$prodspagina);
			$modulo=$numItems % $prodspagina;
			if (($modulo) == 0){
				$pagTotal=($numItems/$prodspagina)-1;
			}
			for ($i=0; $i <= $pagTotal; $i++) { 
				$clase='';
				if ($pag==$i) {
					$clase='uk-badge bg-primary color-white';
				}
				$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&pag='.($i).'&prodspagina='.$prodspagina;
				echo '<li><a href="'.$link.'" class="'.$clase.'">'.($i+1).'</a></li>';
			}
			if ($pag!=$pagTotal AND $numItems!=0) {
				$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&pag='.($pag+1).'&prodspagina='.$prodspagina;
				echo'
				<li><a href="'.$link.'">Siguiente &nbsp;&nbsp; <i class="fa fa-lg fa-angle-right"></i></a></li>';
			}
			?>

			</ul>
		</div>
		<div class="uk-text-right" style="margin-top: -10px;">
			<select name="prodspagina" data-placeholder="Productos por página" id="prodspagina" class="chosen-select uk-select" style="width:120px;">
				<?php
				$arreglo = array(5=>5,20=>20,50=>50,100=>100,500=>500,9999=>"Todos");
				foreach ($arreglo as $key => $value) {
					$checked='';
					if ($key==$prodspagina) {
						$checked='selected';
					}
					echo '
					<option value="'.$key.'" '.$checked.'>'.$value.'</option>';
				}
				?>
				
			</select>
		</div>
	</div>
</div><!-- PAGINATION -->




<div style="min-height:300px;">
</div>



<div>
	<div id="buttons">
		<a href="index.php?seccion=<?=$seccion?>&subseccion=nuevo" class="uk-icon-button uk-button-primary uk-box-shadow-large" uk-icon="icon:plus;ratio:1.4;"></a>
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>



<div id="nuevacat" uk-modal="center: true">
	<div class="uk-modal-dialog uk-modal-body">
		<a class="uk-modal-close uk-close"></a>
		<form action="index.php" class="uk-width-1-1 uk-text-center uk-form" method="post" name="editar" onsubmit="return checkForm(this);">

			<input type="hidden" name="nuevacategoria" value="1">
			<input type="hidden" name="seccion" value="<?=$seccion?>">
			<input type="hidden" name="subseccion" value="categorias">

			<label for="categoria">Nombre de la nueva categoría</label><br><br>
			<input type="text" class="uk-input" name="categoria" tabindex="10" required><br><br>
			<input type="submit" name="send" value="Agregar" tabindex="10" class="uk-button uk-button-primary">
		</form>
	</div>
</div>

<?php 
$scripts='
	// Eliminar producto
	$(".eliminaprod").click(function() {
		var id = $(this).attr(\'data-id\');
		//console.log(id);
		var statusConfirm = confirm("Realmente desea eliminar este Producto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&borrarPod&id="+id);
		} 
	});

	$("#prodspagina").change(function(){
		var prodspagina = $(this).val();
		window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&prodspagina="+prodspagina);
	})

	$(".search").keypress(function(e) {
		if(e.which == 13) {
			var campo = $(this).attr("data-campo");
			var valor = $(this).val();
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=search&campo="+campo+"&valor="+valor);
		}
	});

	';
?>

