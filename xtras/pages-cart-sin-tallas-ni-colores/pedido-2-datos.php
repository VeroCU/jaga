<!DOCTYPE html>
<html lang="<?=$languaje?>">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
  <meta charset="utf-8">
  <title><?=$title?></title>
  <meta name="description" content="<?=$description?>">
  
  <meta property="og:type" content="website">
  <meta property="og:title" content="<?=$title?>">
  <meta property="og:description" content="<?=$description?>">
  <meta property="og:url" content="<?=$rutaEstaPagina?>">
  <meta property="og:image" content="<?=$ruta.$logoOg?>">
  <meta property="fb:app_id" content="<?=$appID?>">

  <?=$headGNRL?>

</head>

<body>

<?=$header?>

<div class="uk-container">
  <div class="uk-width-1-1 margen-top-50">
    <div id="revisardatos">
      <div>
        <p class="text-xl">Revise que todos los datos sean correctos</p>
      </div>
      <div class="uk-child-width-1-3@m" uk-grid>
        <div>
          <div class="uk-card uk-card-body">
            <div>
              <h2>Datos de cliente <a href="#editadatospersonales" uk-toggle><i class="fas fa-lg fa-edit uk-float-right"></i></a></h2>
            </div>
            <div>
              <label for="nombre" class="uk-form-label uk-text-capitalize">nombre</label>
              <?=$row_USER['nombre']?>
            </div>
            <div>
              <label for="email" class="uk-form-label uk-text-capitalize">email</label>
              <?=$row_USER['email']?>
            </div>
            <div>
              <label for="empresa" class="uk-form-label uk-text-capitalize">empresa</label>
              <?=$row_USER['empresa']?>
            </div>
            <div>
              <label for="rfc" class="uk-form-label uk-text-uppercase">rfc</label>
              <?=$row_USER['rfc']?>
            </div>
          </div>
        </div>
        <div>
          <div class="uk-card">
            <div class="uk-card-body">
              <div>
                <h2>Domicilio <a href="#editadomicilio1" uk-toggle><i class="fas fa-lg fa-edit uk-float-right"></i></a></h2>
              </div>
              <div>
                <?=$row_USER['calle']?> #<?=$row_USER['noexterior']?> <?=$row_USER['nointerior']?>
              </div>
              <div>
                <span class="uk-text-muted">Entre</span> &nbsp; <?=$row_USER['entrecalles']?>
              </div>
              <div>
                <?=$row_USER['colonia']?>, <?=$row_USER['cp']?>
              </div>
              <div>
                <?=$row_USER['municipio']?>, <?=$row_USER['estado']?>, <?=$row_USER['pais']?>
              </div>
              <!--<div class="uk-margin">
                <i class="domchange far fa-lg fa-circle uk-text-muted pointer" id="dom1" data-dom="1"></i> Envío a este domicilio
              </div>-->
            </div>
          </div>
        </div>
        <?php 
        if(strlen($row_USER['calle2'])>1){ 
          echo '
        <div>
          <div class="uk-card">
            <div class="uk-card-body">
              <div>
                <h2>Domicilio <a href="#editadomicilio2" uk-toggle><i class="fas fa-lg fa-edit uk-float-right"></i></a></h2>
              </div>
              <div>
                '.$row_USER['calle2'].' #'.$row_USER['noexterior2'].' '.$row_USER['nointerior2'].'
              </div>
              <div>
                <span class="uk-text-muted">Entre</span> &nbsp; '.$row_USER['entrecalles2'].'
              </div>
              <div>
                '.$row_USER['colonia2'].', '.$row_USER['cp2'].'
              </div>
              <div>
                '.$row_USER['municipio2'].', '.$row_USER['estado2'].', '.$row_USER['pais2'].'
              </div>
              <div class="uk-margin">
                <i class="domchange fas fa-lg fa-check-circle color-primary pointer" id="dom2" data-dom="2"></i> Envío a este domicilio
              </div>
            </div>
          </div>
        </div>';

        }else{
          echo '<!--
          <div class="uk-card">
            <div class="uk-card-body">
              <button id="editarbutton" class="uk-button uk-button-default border-orange">Agregar domicilio</button>
            </div>
          </div>-->';
        }
        ?>

      </div>
    </div>
  </div>
  <div class="uk-width-1-1 margen-top-50">
    <div class="uk-panel uk-panel-box">
      <h3 class="uk-text-center"><i class="uk-icon uk-icon-small uk-icon-check-square-o"></i> &nbsp; Productos y cantidades:</h3>
      <table class="uk-table uk-table-striped uk-table-hover uk-table-middle" id="tabla">
        <thead>
          <tr>
            <td >Producto</td>
            <td width="100px" class="uk-text-right">Cantidad</td>
            <td width="100px" class="uk-text-right">Precio</td>
            <td width="100px" class="uk-text-right">Importe</td>
          </tr>
        </thead>
        <tbody>

          <?php
          $subtotal=0;
          $num=0;
          if(isset($_SESSION['carro'])){
            foreach ($arreglo as $key) {

              $prodId=$key['Id'];
              
              $CONSULTA1 = $CONEXION -> query("SELECT * FROM productos WHERE id = $prodId");
              $row_CONSULTA1 = $CONSULTA1 -> fetch_assoc();
              
              $link=$prodId.'_'.urlencode(str_replace($caracteres_no_validos,$caracteres_si_validos,html_entity_decode(strtolower($row_CONSULTA1['titulo'])))).'-.html';

              $importe=$row_CONSULTA1['precio']*$key['Cantidad'];
              $subtotal+=$importe;

              echo '
              <tr>
                <td>
                  <a href="'.$link.'" class="color-general" target="_blank">'.$row_CONSULTA1['sku'].' - '.$row_CONSULTA1['titulo'].'</a>
                </td>
                <td class="uk-text-right">
                  '.$key['Cantidad'].'
                </td>
                <td class="uk-text-right">
                  '.number_format($row_CONSULTA1['precio'],2).'
                </td>
                <td class="uk-text-right">
                  '.number_format($importe,2).'
                </td>
              </tr>';

              $num++;
            }
          }

          $envio=$shipping*$carroTotalProds;
          $subtotal=$subtotal+$envio+$shippingGlobal;
          $iva=($taxIVA>0)?$subtotal*$taxIVA:0;
          $total=$subtotal+$iva;

          if ($total>0) {
            if ($shippingGlobal>0) {
              echo '
              <tr>
                <td style="text-align: left;">
                  Envío global
                </td>
                <td style="text-align: right; ">
                  1
                </td>
                <td style="text-align: right; ">
                  '.number_format($shippingGlobal,2).'
                </td>
                <td style="text-align: right; ">
                  '.number_format($shippingGlobal,2).'
                </td>
              </tr>';
            }
            if ($shipping>0) {
              echo '
              <tr>
                <td style="text-align: left; ">
                  Envío por pieza
                </td>
                <td style="text-align: right; ">
                  '.$carroTotalProds.'
                </td>
                <td style="text-align: right; ">
                  '.number_format($shipping,2).'
                </td>
                <td style="text-align: right; ">
                  '.number_format($envio,2).'
                </td>
              </tr>';
            }

            if($taxIVA>0){
              echo '
              <tr>
                <td colspan="3" class="uk-text-right">
                  Subtotal
                </td>
                <td class="uk-text-right">
                  '.number_format($subtotal,2).'
                </td>
              </tr>
              <tr>
                <td colspan="3" class="uk-text-right">
                  IVA
                </td>
                <td class="uk-text-right">
                  '.number_format($iva,2).'
                </td>
              </tr>
              <tr>
                <td colspan="3" class="uk-text-right">
                  Total
                </td>
                <td class="uk-text-right">
                  '.number_format($total,2).'
                </td>
              </tr>
              ';
            }else{
              echo '
              <tr>
                <td colspan="3" class="uk-text-right">
                  Total
                </td>
                <td class="uk-text-right">
                  '.number_format($subtotal,2).'
                </td>
              </tr>';
            }
          }
    echo '
        </tbody>
      </table>
    </div>
  </div>
  <div class="uk-width-1-1 uk-text-center padding-v-50">
    <div uk-grid class="uk-child-width-1-2@s">
      <div class="uk-text-right@m uk-text-center">
        <button data-enlace="procesar-deposito" class="siguiente uk-button uk-button-personal">Depósito o transferencia</button>
      </div>
      <div class="uk-text-left@m uk-text-center">
        <button data-enlace="procesar-pedido" class="siguiente uk-button uk-button-personal">Paga con PayPal</button>
      </div>
    </div>
    <div uk-grid class="uk-child-width-1-2@s">
      <div class="uk-text-right@m uk-text-center">
        <img src="img/design/pago-oxxo.jpg">
      </div>
      <div class="uk-text-left@m uk-text-center">
        <img src="img/design/pago-paypal.jpg">
      </div>
    </div>
  </div>';
?>

</div>

<div class="padding-v-50">
</div>

<?=$footer?>


<div id="editadatospersonales" uk-modal>
  <div class="uk-modal-dialog uk-modal-body">
    <button class="uk-modal-close-default" type="button" uk-close></button>
    <div>
      <h2>Editar atos personales</h2>
    </div>
    <div>
      <label for="nombre" class="uk-form-label uk-text-capitalize">nombre</label>
      <input type="text" value="<?=$row_USER['nombre']?>" class="uk-input">
    </div>
    <div>
      <label for="email" class="uk-form-label uk-text-capitalize">email</label>
      <input type="text" value="<?=$row_USER['email']?>" class="uk-input">
    </div>
    <div>
      <label for="empresa" class="uk-form-label uk-text-capitalize">empresa</label>
      <input type="text" value="<?=$row_USER['empresa']?>" class="uk-input">
    </div>
    <div>
      <label for="rfc" class="uk-form-label uk-text-uppercase">rfc</label>
      <input type="text" value="<?=$row_USER['rfc']?>" class="uk-input uk-text-uppercase">
    </div>
    <div class="uk-margin uk-text-center">
      <button class="uk-button uk-button-default uk-modal-close" type="button">Cerrar</button>
      <button class="uk-button uk-button-personal">Guardar</button>
    </div>
  </div>
</div>

<div id="editadomicilio1" uk-modal>
  <div class="uk-modal-dialog uk-modal-body">
    <button class="uk-modal-close-default" type="button" uk-close></button>
    <div>
      <h2>Domicilio</h2>
    </div>
    <div>
      <label for="calle" class="uk-form-label uk-text-capitalize">calle</label>
      <input type="text" data-campo="calle" value="<?=$row_USER['calle']?>" class="editaruser uk-input uk-input-grey" >
    </div>
    <div>
      <label for="noexterior" class="uk-form-label uk-text-capitalize">no. exterior</label>
      <input type="text" data-campo="noexterior" value="<?=$row_USER['noexterior']?>" class="editaruser uk-input uk-input-grey" >
    </div>
    <div>
      <label for="nointerior" class="uk-form-label uk-text-capitalize">no. interior</label>
      <input type="text" data-campo="nointerior" value="<?=$row_USER['nointerior']?>" class="editaruser uk-input uk-input-grey">
    </div>
    <div>
      <label for="entrecalles" class="uk-form-label uk-text-capitalize">entrecalles</label>
      <input type="text" data-campo="entrecalles" value="<?=$row_USER['entrecalles']?>" class="editaruser uk-input uk-input-grey" >
    </div>
    <div>
      <label for="pais" class="uk-form-label uk-text-capitalize">pais</label>
      <input type="text" data-campo="pais" value="<?=$row_USER['pais']?>" class="editaruser uk-input uk-input-grey" >
    </div>
    <div>
      <label for="estado" class="uk-form-label uk-text-capitalize">estado</label>
      <input type="text" data-campo="estado" value="<?=$row_USER['estado']?>" class="editaruser uk-input uk-input-grey" >
    </div>
    <div>
      <label for="municipio" class="uk-form-label uk-text-capitalize">municipio</label>
      <input type="text" data-campo="municipio" value="<?=$row_USER['municipio']?>" class="editaruser uk-input uk-input-grey" >
    </div>
    <div>
      <label for="colonia" class="uk-form-label uk-text-capitalize">colonia</label>
      <input type="text" data-campo="colonia" value="<?=$row_USER['colonia']?>" class="editaruser uk-input uk-input-grey" >
    </div>
    <div>
      <label for="cp" class="uk-form-label uk-text-uppercase">cp</label>
      <input type="text" data-campo="cp" value="<?=$row_USER['cp']?>" class="editaruser uk-input uk-input-grey" >
    </div>
  </div>
</div>

<div id="editadomicilio2" uk-modal>
  <div class="uk-modal-dialog uk-modal-body">
    <div>
      <h2>Domicilio 2</h2>
    </div>
    <div>
      <label for="calle" class="uk-form-label uk-text-capitalize">calle</label>
      <input type="text" data-campo="calle2" value="<?=$row_USER['calle2']?>" class="editaruser uk-input uk-input-grey">
    </div>
    <div>
      <label for="noexterior" class="uk-form-label uk-text-capitalize">no. exterior</label>
      <input type="text" data-campo="noexterior2" value="<?=$row_USER['noexterior2']?>" class="editaruser uk-input uk-input-grey">
    </div>
    <div>
      <label for="nointerior" class="uk-form-label uk-text-capitalize">no. interior</label>
      <input type="text" data-campo="nointerior2" value="<?=$row_USER['nointerior2']?>" class="editaruser uk-input uk-input-grey">
    </div>
    <div>
      <label for="entrecalles" class="uk-form-label uk-text-capitalize">entrecalles</label>
      <input type="text" data-campo="entrecalles2" value="<?=$row_USER['entrecalles2']?>" class="editaruser uk-input uk-input-grey">
    </div>
    <div>
      <label for="pais" class="uk-form-label uk-text-capitalize">pais</label>
      <input type="text" data-campo="pais2" value="<?=$row_USER['pais2']?>" class="editaruser uk-input uk-input-grey">
    </div>
    <div>
      <label for="estado" class="uk-form-label uk-text-capitalize">estado</label>
      <input type="text" data-campo="estado2" value="<?=$row_USER['estado2']?>" class="editaruser uk-input uk-input-grey">
    </div>
    <div>
      <label for="municipio" class="uk-form-label uk-text-capitalize">municipio</label>
      <input type="text" data-campo="municipio2" value="<?=$row_USER['municipio2']?>" class="editaruser uk-input uk-input-grey">
    </div>
    <div>
      <label for="colonia" class="uk-form-label uk-text-capitalize">colonia</label>
      <input type="text" data-campo="colonia2" value="<?=$row_USER['colonia2']?>" class="editaruser uk-input uk-input-grey">
    </div>
    <div>
      <label for="cp" class="uk-form-label uk-text-uppercase">cp</label>
      <input type="text" data-campo="cp2" value="<?=$row_USER['cp2']?>" class="editaruser uk-input uk-input-grey">
    </div>
  </div>
</div>


<div id="spinner" class="uk-modal-full" uk-modal>
  <div class="uk-modal-dialog uk-flex uk-flex-center uk-flex-middle uk-height-viewport">
    <div>
      <div uk-spinner="ratio: 5">
      </div>
      <div class="uk-text-center" style="padding-top:50px;">
        <img src="img/design/logo.png" style="max-width:150px;">
      </div>
    </div>
  </div>
</div>


<?=$scriptGNRL?>

<script>
  $('.siguiente').click(function(){
    var datos = $(this).data();
    UIkit.modal("#spinner").show();
    window.location = (datos.enlace);
  });

  $('#dom2').click(function(){
    var dom2 = $(this).attr("data-dom2");
    if (dom2==0) {
      dom2 = 1;
      $(this).removeClass("fa-square");
      $(this).removeClass("far");
      $(this).removeClass("uk-text-muted");
      $(this).addClass("fa-check");
      $(this).addClass("fas");
      $(this).addClass("color-verde");
    }else{
      dom2 = 0;
      $(this).removeClass("fa-check");
      $(this).removeClass("fas");
      $(this).removeClass("color-verde");
      $(this).addClass("fa-square");
      $(this).addClass("far");
      $(this).addClass("uk-text-muted");
    }
    $(this).attr("data-dom2",dom2);

  });
</script>

</body>
</html>