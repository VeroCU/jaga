<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>
<section uk-slider>

    <div class="uk-position-relative">

        <div class="uk-slider-container uk-light">
            <ul class="uk-slider-items uk-child-width-1-1 uk-align-center">
                <li class="uk-width-1-1 half">
                    <div class="half uk-position-center uk-panel uk-cover-container uk-height-medium uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(./img/design/bg_slider.png);">
                    </div>
                </li>
            </ul>
        </div>

        <div class="uk-hidden@s uk-light">
            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>

        <div class="uk-visible@s">
            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>

    </div>

    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
</section>


<!--CATEGORIAS -->
<?=$categorias?>


<div class="uk-width-1-1 uk-text-center padding-top-60">
	<h3 class=""> Manda un mensaje. Nosotros te podemos asesorar.</h3>
</div>			
<section class="uk-container uk-container-small uk-flex uk-flex-middle">
	<div class="uk-width-1-1">
		<div uk-grid class="uk-child-width-1-2@s">
			<div>
				<input type="text" class="uk-input input-personal padding-v-20" id="footernombre" placeholder="NOMBRE"></label>
			</div>
			<div>
				<input type="email" class="uk-input input-personal padding-v-20" id="footeremail" placeholder="CORREO"></label>
			</div>
			<div>
				<input type="text" class="uk-input input-personal padding-v-20" id="footertelefono" placeholder="TELÉFONO/CEL"></label>
			</div>
			<div>
				<textarea type="text" class="uk-textarea input-personal uk-grid-collapse" id="footercomentarios" rows="1" placeholder="MENSAJE"
				style="resize:none; white-space:nowrap; overflow-x:scroll;"></textarea>
			</div>
		</div>
		<div class="margin-top-20 uk-text-center uk-align-right uk-grid-collapse" style="margin:20px 0">
			<button class="uk-button" id="footersend" style="background: transparent;color:#e07134;font-weight:400">
				Enviar <span uk-icon="icon:  chevron-right; ratio: 1.2"></span>
			</button>
		</div>
	</div>
</section> 

<section class="uk-width-1-1 padding-v-20">
	<div class="uk-grid-collapse margin-top-100">
		<div class="uk-flex uk-flex-center uk-position-relative">
			<div class="uk-width-1-1@s uk-width-1-3@m title_gris">
				<h1 class="uk-grid-collapse uk-text-uppercase padding-top-20 uk-text-center title_gris_h1">PROMOCIONES</h1>
			</div>
		</div>
	</div>
	<hr class="uk-width-1-1 title_gris_hr">
</section>

<section class="uk-width-1-1 padding-v-20">
	<div class="uk-container">
		<div class="margin-left-0" uk-grid>

			<div class="uk-width-1-1 padding-left-0" uk-slider>
			    <div class="uk-position-relative">
			        <div class="uk-slider-container uk-light">
			            <ul class="uk-slider-items uk-child-width-1-1@s uk-child-width-1-3@s uk-child-width-1-4@m">
			                <?php for ($i=0; $i < 8; $i++): ?> 
							<li>
								<div class="padding-left-0 products">
								<a href="detalle">
									<div class="container_product">
										<div class="oswald uk-width-1-2 uk-text-center btn_ver">VER DETALLES</div>
										<div class="uk-position-relative uk-width-auto product_border">
											<div class="uk-position-relative product_border_in">
												<div class="uk-flex uk-flex-center uk-flex-middle">
													<div class="uk-flex uk-flex-middle uk-cover-container">
														<div class="uk-cover-container uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle product img_product" 
															style="background-image: url(./img/design/producto1.jpg);"></div>
													</div>
												</div>
											</div>
										</div>
										<p class="oswald uk-text-center uk-text-uppercase uk-text-truncate">Nombre de el producto</p>
									</div>
								</a>
								</div>
							</li>
							<?php endfor ?>
			            </ul>
			        </div>

			        <div class="uk-hidden@s uk-light">
			            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
			            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
			        </div>

			        <div class="uk-visible@s">
			            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
			            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
			        </div>
			    </div>
			    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
			</div>

		</div>
	</div>
</section>


<section class="uk-width-1-1 uk-aling-center margin-top-100">
	<div class="uk-container">
		<div class="margin-left-0" uk-grid>
			<div class="uk-width-1-1@s uk-width-1-2@m padding-left-0" style="border:solid 3px #7c330a">
				<div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(./img/design/img1.jpg);min-height:400px">
        		</div>
			</div>
			<div class="uk-width-1-1@s uk-width-1-2@m" style="background:#7c330a; border:solid 3px #7c330a;padding-left: 0;">
				<div class="uk-width-1-1" style="border-top:solid 8px #fff;">
					<div class="padding-60"style="color:#fff;">
						<h1 class="uk-text-uppercase" style="color:#fff">JAGA</h1>
						<p>Dedicados a la fabricación y comercialización de equipos agropecuarios de calidad.</p>
					</div>
					<div uk-grid>
						<div class="uk-width-expand@s uk-width-1-2@m" style="border-bottom:solid 2px #fff">
							<div class="margin-top-20 uk-text-center uk-align-right uk-grid-collapse" style="margin:0">
								<button class="uk-button oswald" id="" style="background:transparent;color:#fff;font-weight:400;padding-right:0">
									VER MÁS <span uk-icon="icon:  chevron-right; ratio: 1.2"></span>
								</button>
							</div>
						</div>
						<div class="uk-width-1-4@s uk-width-1-2@m"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="uk-width-1-1 padding-v-20">
	<div class="uk-grid-collapse margin-top-150">
		<div class="uk-flex uk-flex-center uk-position-relative">
			<div class="uk-width-1-3 title_gris">
				<h1 class="uk-grid-collapse uk-text-uppercase padding-top-20 uk-text-center title_gris_h1">CLIENTES</h1>
			</div>
		</div>
	</div>
	<hr class="uk-width-1-1 title_gris_hr">
</section>

<section class="uk-width-1-1 padding-v-20">
	<div class="uk-container">
		<div class="" uk-grid>

			<div class="uk-width-1-1" uk-slider>
			    <div class="uk-position-relative">

			        <div class="uk-slider-container uk-light">
			            <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-4@m">
			                <?php for ($i=1; $i < 5; $i++): ?> 
							<li>
								<div>
									<img src="./img/design/cliente<?= $i ?>.png">
								</div>
							</li>
							<?php endfor ?>
			            </ul>
			        </div>

			        <div class="uk-hidden@s uk-light">
			            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
			            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
			        </div>
			        <div class="uk-visible@s">
			            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
			            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
			        </div>
			    </div>
			    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
			</div>

		</div>
	</div>
</section>

	
<footer class="margin-top-100">
	<?=$footer?>
</footer>

<?=$scriptGNRL?>

</body>
</html>