<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>
<section uk-slider>

    <div class="uk-position-relative">

        <div class="uk-slider-container uk-light">
            <ul class="uk-slider-items uk-child-width-1-1 uk-align-center">
                <li class="uk-width-1-1 half">
                    <div class="half uk-position-center uk-panel uk-cover-container uk-height-medium uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(./img/design/bg_slider.png);">
                    </div>
                </li>
            </ul>
        </div>

        <div class="uk-hidden@s uk-light">
            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>

        <div class="uk-visible@s">
            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>

    </div>

    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
</section>

<section class="uk-width-1-1" style="background:#ededed">
	<section class="uk-width-1-1">
		<div class="uk-container padding-v-60">
			<div class="margin-left-0" uk-grid>
				<div class="uk-width-1-3@m">
					<div class="uk-width-1-1 padding-left-0 products">
					
						<div class="container_product_detalle">
							<div class="uk-position-relative uk-width-auto product_border">
								<div class="uk-position-relative product_border_in">
									<div class="uk-flex uk-flex-center uk-flex-middle">
										<div class="uk-flex uk-flex-middle uk-cover-container">
											<img class="padding-40" src="./img/design/producto1.jpg">
										</div>
									</div>
								</div>
							</div>
						</div>
					
					</div>
					<div class="uk-width-1-1" style="padding-top:10px">
						<div class="uk-grid margin-left-0" uk-grid>
							<div class="uk-width-1-3 uk-padding-small mini">
								<img class="" src="./img/design/producto1.jpg">
							</div>
							<div class="uk-width-1-3  uk-padding-small mini">
								<img class="" src="./img/design/producto1.jpg">
							</div>
							<div class="uk-width-1-3 uk-padding-small mini">
								<img class="" src="./img/design/producto1.jpg">
							</div>
						</div>
					</div>
				</div>
				<div class="uk-width-expand padding-top-90">
					<div class="uk-container uk-container-small">
						<div class="uk-grid-small text-we" uk-grid style="margin-left:0;">
							<div class="uk-width-1-1" style="padding-left:0">
								
								<div class="uk-grid-collapse" uk-grid>
									<div class="uk-flex uk-flex-left uk-position-relative">
										<div class="uk-width-1-1 title_brown">
											<h3 class="uk-grid-collapse uk-text-uppercase uk-text-center name" style="">NOMBRE DE EL PRODUCTO</h3>
										</div>
									</div>
								</div>

							</div>
						</div>
						<div class="uk-grid-small text-we padding-h-50" uk-grid style="margin-top:0;margin-left:0">
							<p class="margin-v-50" style="min-height: 230px; overflow: hidden">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</section>

<section  class="uk-width-1-1" style="margin-top:-28px; padding-bottom:42px;">
	<div class="uk-container uk-container-small">
		<div class="uk-grid-small" uk-grid>
			<div class="uk-width-1-1@s uk-width-1-4@m uk-align-center uk-text-center" style="background:#7c330a; color:#fff;font-size:20px;padding-top:10px;padding-bottom:0;z-index:2">
				CARACTERISTICAS:
			</div>
			<div class="uk-width-expand"></div>
			<div class="uk-width-1-1@s uk-width-1-2@m">
				<div class="uk-flex uk-flex-center uk-position-relative title_pestana_invert">
					<div class="uk-width-1-1 title_pestana_container">
						<div class="uk-grid-small" uk-grid>	
							<h1 class="uk-width-expand uk-grid-collapse uk-text-uppercase padding-top-20 uk-text-center title_pestana_h1">DESCARGAR <br>FICHA TÉCNICAS</h1>
							<span class="uk-width-auto" style="color:#e07134" uk-icon="icon: pull; ratio: 2"></span>
							<span class="uk-width-auto"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="uk-width-1-1 padding-v-30" style="background:#e07134">
	<div class="uk-width-1-1 padding-v-20"> </div>	
</section>

<section class="uk-width-1-1 padding-v-20">
	<div class="uk-grid-collapse margin-top-100">
		<div class="uk-flex uk-flex-center uk-position-relative">
			<div class="uk-width-1-1@s uk-width-1-3@m title_gris">
				<h1 class="uk-grid-collapse uk-text-uppercase padding-top-20 uk-text-center title_gris_h1" style="margin-top: -100px;">PRODUCTOS<br>RELACIONADOS</h1>
			</div>
		</div>
	</div>
	<hr class="uk-width-1-1 title_gris_hr">
</section>
<section class="uk-width-1-1 padding-v-20">
	<div class="uk-container">
		<div class="margin-left-0" uk-grid>

			<div class="uk-width-1-1 padding-left-0" uk-slider>
			    <div class="uk-position-relative">
			        <div class="uk-slider-container uk-light">
			            <ul class="uk-slider-items uk-child-width-1-1@s uk-child-width-1-3@s uk-child-width-1-4@m">
			                <?php for ($i=0; $i < 8; $i++): ?> 
							<li>
								<div class="padding-left-0 products">
								<a href="#">
									<div class="container_product">
										<div class="oswald uk-width-1-2 uk-text-center btn_ver">VER DETALLES</div>
										<div class="uk-position-relative uk-width-auto product_border">
											<div class="uk-position-relative product_border_in">
												<div class="uk-flex uk-flex-center uk-flex-middle">
													<div class="uk-flex uk-flex-middle uk-cover-container">
														<img src="./img/design/producto1.jpg">
													</div>
												</div>
											</div>
										</div>
										<p class="oswald uk-text-center uk-text-uppercase uk-text-truncate">Nombre de el producto</p>
									</div>
								</a>
								</div>
							</li>
							<?php endfor ?>
			            </ul>
			        </div>

			        <div class="uk-hidden@s uk-light">
			            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
			            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
			        </div>

			        <div class="uk-visible@s">
			            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
			            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
			        </div>
			    </div>
			    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
			</div>

		</div>
	</div>
</section>


<footer class="margin-top-100">
	<?=$footer?>
</footer>

<?=$scriptGNRL?>

</body>
</html>