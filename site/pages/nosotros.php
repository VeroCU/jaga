<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>
<section uk-slider>

    <div class="uk-position-relative">

        <div class="uk-slider-container uk-light">
            <ul class="uk-slider-items uk-child-width-1-1 uk-align-center">
                <li class="uk-width-1-1 half">
                    <div class="half uk-position-center uk-panel uk-cover-container uk-height-medium uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(./img/design/bg_slider1.png);">
                    </div>
                </li>
            </ul>
        </div>

        <div class="uk-hidden@s uk-light">
            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>

        <div class="uk-visible@s">
            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>

    </div>

    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
</section>

<section class="uk-width-1-1 padding-top-90 margin-bottom-120 nosotros_quienes">
	<div class="uk-container uk-container-small">
		<div class="uk-grid-small text-we quienes_0" uk-grid>
			<div class="uk-width-1-1">
				<div class="uk-grid-collapse">
					<div class="uk-flex uk-flex-center uk-position-relative">
						<div class="uk-width-11@s uk-width-1-2@m title_brown">
							<h3 class="uk-grid-collapse uk-text-uppercase uk-text-center">¿Quiénes somos?</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="uk-grid-small text-we padding-h-50 quienes_text" uk-grid>
			<p class="margin-v-50">
				Somos una empresa con más de 40 años, fundada en los años 70’s por su dueño el Ing. J. Javier García V. y dedicada principalmente en la fabricación de productos y al desarrollo de la industria agropecuaria. A través de todo este tiempo se ha actualizado y ha procurado estar al día con los diversos cambios y tendencias de la industria.
			</p>
		</div>
	</div>
</section>

<section class="uk-width-1-1 uk-aling-center" style="margin-top:-300px">
	<div class="uk-container">
		<div class="margin-left-0" uk-grid>
			<div class="uk-width-1-1@s uk-width-1-2@m padding-left-0" style="border:solid 3px #7c330a">
				<div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(./img/design/img1.jpg);min-height:400px">
        		</div>
			</div>
			<div class="uk-width-1-1@s uk-width-1-2@m" style="background:#7c330a; border:solid 3px #7c330a;padding-left: 0;">
				<div class="uk-width-1-1" style="border-top:solid 8px #fff;">
					<div class="padding-60"style="color:#fff;">
						<h1 class="uk-text-uppercase" style="color:#fff">Dedicados...</h1>
						<p>a la fabricación y comercialización de equipos agropecuarios de calidad.</p>
					</div>
					<div uk-grid>
						<div class="uk-width-expand@s uk-width-1-2@m" style="border-bottom:solid 2px #fff">
							<div class="margin-top-20 uk-text-center uk-align-right uk-grid-collapse" style="margin:0">
								<button class="uk-button oswald" id="" style="background:transparent;color:#fff;font-weight:400;padding-right:0">
									VER MÁS <span uk-icon="icon:  chevron-right; ratio: 1.2"></span>
								</button>
							</div>
						</div>
						<div class="uk-width-1-4@s uk-width-1-2@m"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="margin-top-100 nosotros_valores">
	<div class="uk-grid uk-grid-collapse" uk-grid>
		<div class="uk-width-1-1@s uk-width-expand@m valor1">
			<div class="uk-grid uk-padding" uk-grid>
				<div class="uk-width-1-1@s uk-width-1-4@m"> </div>
				<div class="uk-width-expand">
					<div class="uk-grid uk-grid-collapse" uk-grid>
						<div class="uk-width-1-1@s uk-width-1-5@m img">
							<img src="./img/design/icon_mision.png">
						</div>
						<div class="uk-width-expand container_text">
							<div class="oswald title">
								MISIÓN
							</div>
							<div class="text">
								Buscar la mayor satisfacción de nuestros clientes (calidad y         servicio)
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="uk-width-1-1@s uk-width-auto@m" style=""><div style="padding:0 30px">&nbsp;</div></div>
		<div class="uk-width-1-1@s uk-width-expand@m valor2">
			<div class="uk-grid uk-padding" uk-grid>
				<div class="uk-width-expand">
					<div class="uk-grid uk-grid-collapse" uk-grid>
						<div class="uk-width-1-1@s uk-width-1-5@m img">
							<img src="./img/design/icon_vision.png">
						</div>
						<div class="uk-width-expand container_text">
							<div class="oswald title">
								VISIÓN
							</div>
							<div class="text">
								Consolidarnos como una empresa industrialmente productiva y representativa en el ramo agropecuario.
							</div>
						</div>
					</div>
				</div>
				<div class="uk-width-1-1@s uk-width-1-4@m"> </div>
			</div>
		</div>
	</div>
</section>

<section class="uk-width-1-1 uk-aling-center" style="margin-top:100px">
	<div class="uk-width-1-1 uk-aling-center" style="background:#e07134;border-bottom:solid 10px #7c330a;">
		<div class="uk-grid-collapse">
			<div class="uk-flex uk-flex-center uk-position-relative">
				<div class="uk-width-11@s uk-width-1-5@m title_brown">
					<h3 class="uk-grid-collapse uk-text-uppercase uk-text-center">
						VALORES
					</h3>
				</div>
			</div>
		</div>
		<div class="uk-container uk-container-small" style="padding:60px 20px">
			<div class="margin-left-0" uk-grid>
				<div class="uk-width-1-1@s uk-width-1-5@m" style="border:solid 3px #7c330a">
					<div class="uk-text-uppercase" uk-grid style="padding:0;">
						<div class="uk-width-1-1 uk-flex uk-flex-center" style="margin-bottom: 0; padding-left:0; margin-top:-14px;"> 
							<div style="background:#7c330a;width:20px;height:20px;border-radius:100%"></div> 
						</div>
						<div  class="uk-width-1-1 uk-text-center"
						style="color:#7c330a;font-weight:600;margin:0;padding:10px;">
							HONESTIDAD
						</div>
					</div>
				</div>
				<div class="uk-width-1-1@s uk-width-1-5@m uk-aling-center"></div>
				<div class="uk-width-1-1@s uk-width-1-5@m" style="border:solid 3px #7c330a">
					<div class="uk-text-uppercase" uk-grid style="padding:0;">
						<div class="uk-width-1-1 uk-flex uk-flex-center" style="margin-bottom: 0; padding-left:0; margin-top:-14px;"> 
							<div style="background:#7c330a;width:20px;height:20px;border-radius:100%"></div> 
						</div>
						<div  class="uk-width-1-1 uk-text-center"
						style="color:#7c330a;font-weight:600;margin:0;padding:10px;">
							COMPROMISO
						</div>
					</div>
				</div>
				<div class="uk-width-1-1@s uk-width-1-5@m uk-aling-center"></div>
				<div class="uk-width-1-1@s uk-width-1-5@m" style="border:solid 3px #7c330a">
					<div class="uk-text-uppercase" uk-grid style="padding:0;">
						<div class="uk-width-1-1 uk-flex uk-flex-center" style="margin-bottom: 0; padding-left:0; margin-top:-14px;"> 
							<div style="background:#7c330a;width:20px;height:20px;border-radius:100%"></div> 
						</div>
						<div  class="uk-width-1-1 uk-text-center"
						style="color:#7c330a;font-weight:600;margin:0;padding:10px;">
							CALIDAD
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<footer class="margin-top-100">
	<?=$footer?>
</footer>

<?=$scriptGNRL?>

</body>
</html>