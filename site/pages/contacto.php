<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>
<section uk-slider>

    <div class="uk-position-relative">

        <div class="uk-slider-container uk-light">
            <ul class="uk-slider-items uk-child-width-1-1 uk-align-center">
                <li class="uk-width-1-1 half">
                    <div class="half uk-position-center uk-panel uk-cover-container uk-height-medium uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(./img/design/bg_slider1.png);">
                    </div>
                </li>
            </ul>
        </div>

        <div class="uk-hidden@s uk-light">
            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>

        <div class="uk-visible@s">
            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>

    </div>

    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
</section>

<section class="" style="background:#e07134" >
	<div class="uk-container ">
		<div class="uk-grid-collapse" uk-grid>
			<div class="uk-width-1-1 contacto">
                    <h1> CONTÁCTANOS </h1>
					<div class="" uk-grid>
						<div class="uk-width-1-1@s uk-width-1-2@m form">
                            <div class="text">Manda un mensaje. Nosotros te podemos asesorar.</div>
                            <div class="contain-input">
                                <input type="text" name="nombre" placeholder="Nombre">
                            </div>
                            <div class="contain-input">
                                <input type="text" name="nombre" placeholder="Nombre">
                            </div>
                            <div class="contain-input">
                                <input type="text" name="nombre" placeholder="Nombre">
                            </div>
                            <div class="contain-input">
                                <input type="text" name="nombre" placeholder="Nombre">
                            </div>
                            <div class="margin-top-50">
                                <a class="uk-grid-collapse send" href="#">
                                    ENVIAR 
                                    <span uk-icon="icon: chevron-right; ratio: 1.2"></span></a>
                            </div>
                        </div>
						<div class="uk-width-1-1@s uk-width-1-2@m padding-movil">
                            <div uk-grid>
                                <div class="uk-width-1-1@s uk-width-1-2@m datos">
                                    <div class="titles">
                                        Teléfonos
                                    </div> 
                                    <div class="dates">
                                        (33)36354402 </br>
                                        (33)36590244
                                    </div>  
                                </div>
                                <div class="uk-width-1-1@s uk-width-1-2@m datos">
                                    <div class="titles">
                                        Correo
                                    </div>  
                                    <div class="dates">
                                        contacto@jaga.com
                                    </div>  
                                </div>
                            </div>
                            <div uk-grid>
                                <div class="uk-width-1-1@s uk-width-1-2@m datos">
                                    <div class="titles">Dirección</div> 
                                    <div class="dates">
                                        alle San Ángel No. 538,</br>
                                        Col. Valle de la Misericordia.</br>
                                        Tlaquepaque, Jalisco. Mex.
                                    </div>               
                                </div>
                                <div class="uk-width-1-1@s uk-width-1-2@m datos">   
                                    <div class="titles">Lorem</div>  
                                    <div class="dates">
                                        <span uk-icon="icon: whatsapp; ratio: 1.2"></span>
                                        <span uk-icon="icon: facebook; ratio: 1.2"></span>
                                        <span uk-icon="icon: instagram; ratio: 1.2"></span>
                                    </div>                
                                </div>
                            </div>
                        </div>
					</div>
			</div>
		</div>
	</div>
</section>

<section class="uk-width-1-1 uk-container-large">
	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14934.691177541496!2d-103.3965013!3d20.6421898!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbb4c3906c38265fd!2sWozial%20Marketing%20Lovers!5e0!3m2!1ses!2smx!4v1571635783109!5m2!1ses!2smx" width="100%" height="500" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</section>
	
<footer class="">
	<?=$footer?>
</footer>

<?=$scriptGNRL?>

</body>
</html>