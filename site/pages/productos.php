<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>

<div class="uk-width-1-1 margin-top-50"> </div>
<!--CATEGORIAS -->
<?=$categorias?>

<section class="uk-width-1-1 padding-v-20  margin-top-50">
	<div class="uk-container">
		<div class="margin-left-0" uk-grid>
			
        	<?php for ($i=0; $i < 16; $i++): ?> 
			<div class="uk-width-1-4@m">
				<div class="padding-left-0 products">
				<a href="detalle">
					<div class="container_product">
						<div class="oswald uk-width-1-2 uk-text-center btn_ver">VER DETALLES</div>
						<div class="uk-position-relative uk-width-auto product_border">
							<div class="uk-position-relative product_border_in">
								<div class="uk-flex uk-flex-center uk-flex-middle">
									<div class="uk-flex uk-flex-middle uk-cover-container">
										<div class="uk-cover-container uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle product img_product" 
											style="background-image: url(./img/design/producto1.jpg);"></div>
									</div>
								</div>
							</div>
						</div>
						<p class="oswald uk-text-center uk-text-uppercase uk-text-truncate">Nombre de el producto</p>
					</div>
				</a>
				</div>
			</div>
			<?php endfor ?>
			        
		</div>
	</div>
</section>
	
<footer class="margin-top-100">
	<?=$footer?>
</footer>

<?=$scriptGNRL?>

</body>
</html>