<?php
/* %%%%%%%%%%%%%%%%%%%% MENSAJES               */
	if($mensaje!=''){
		$mensajes='
			<div class="uk-container">
				<div uk-grid>
					<div class="uk-width-1-1 margin-v-20">
						<div class="uk-alert-'.$mensajeClase.'" uk-alert>
							<a class="uk-alert-close" uk-close></a>
							'.$mensaje.'
						</div>					
					</div>
				</div>
			</div>';
	}

/* %%%%%%%%%%%%%%%%%%%% RUTAS AMIGABLES        */
		$rutaInicio			=	'Inicio';
		$rutaNosotros       =   'nosotros';
		$rutaProductos      =   'productos';
		$rutaContacto       =   'contacto';
		$rutaPedido			=	$ruta.'Revisar_orden';

/* %%%%%%%%%%%%%%%%%%%% MENU                   */
	$menu='
		<li class="'.$nav1.'"><a href="'.$rutaInicio.'">Inicio</a></li>
		';

	$menuMovil='
		<li><a class="'.$nav1.'" href="'.$ruta.'">INICIO</a></li>
		<li><a class="'.$nav2.'" href="'.$rutaNosotros.'">NOSOTROS</a></li>
		<li class="pad-marg-0"> PRODUCTOS:
			<ul class="uk-container uk-container-large pad-marg-0">
				<li class="pad-marg-0">
					<a class="submenu'.$nav3.'" href="'.$rutaProductos.'">
						<div class="uk-cover-container uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle cat containr-border-circle" style="background-image: url(./img/design/cat1.png);"> </div> 
						Avicola
					</a>
				</li>
				<li class="pad-marg-0">
					<a class="submenu'.$nav3.'" href="'.$rutaProductos.'">
						<div class="uk-cover-container uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle cat containr-border-circle" style="background-image: url(./img/design/cat2.png);"> </div> Porcina
					</a>
				</li>
				<li class="pad-marg-0">
					<a class="submenu'.$nav3.'" href="'.$rutaProductos.'">
						<div class="uk-cover-container uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle cat containr-border-circle" style="background-image: url(./img/design/cat3.png);"> </div> Herrajes
					</a>
				</li>
				<li class="pad-marg-0">
					<a class="submenu'.$nav3.'" href="'.$rutaProductos.'">
						<div class="uk-cover-container uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle cat containr-border-circle" style="background-image: url(./img/design/cat4.png);"> </div> Equipos Automáticos
					</a>
				</li>
				<li class="pad-marg-0">
					<a class="submenu'.$nav3.'" href="'.$rutaProductos.'">
						<div class="uk-cover-container uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle cat containr-border-circle" style="background-image: url(./img/design/cat5.png);"> </div> Equipos de climatización
					</a>
				</li>
			</ul>
		</li>
		<li><a class="'.$nav5.'" href="'.$rutaContacto.'">CONTACTO</a></li>
		';

/* %%%%%%%%%%%%%%%%%%%% HEADER                 */
	$header='
		<div class="uk-offcanvas-content uk-position-relative">
			<header>
				<div class="uk-container uk-padding-small" >
					<div uk-grid class="uk-grid-match">

						<!-- Botones de Logeo -->
						<div class="uk-width-expand" >
							<div uk-grid class="uk-grid-collapse">
								<div class="uk-width-1-3@m">
									<!-- Botón menú móviles -->
									<div class="uk-width-auto oswald menu_hamburguesita">
										<a href="#menu-movil" uk-toggle class="uk-icon-button" style="color:#e07134">
											<i class="fa fa-bars fa-1x" aria-hidden="true"></i></a><span>MENÚ</span>
									</div>
								</div>
								<div class="uk-width-expand@s uk-width-1-3@m for_pc"> 
									<div class="uk-align-center uk-margin-remove-adjacent" style="padding:0 80px">
										<div>
										    <div class="uk-width-1-1 uk-search uk-search-default uk-border-pill" style="border:solid 2px #e07134;">
										        <!--span class="uk-search-icon-flip" style="color:#e07134;" uk-search-icon></span-->
										        <a href="" class="uk-search-icon-flip" uk-search-icon style="color:#e07134"></a>
										        <input class="uk-search-input " type="search" placeholder="¿Qué buscabas?">
										    </div>
										</div>
									</div>
								</div>
								<div class="uk-width-auto@s uk-width-1-3@m">
									<div class="uk-align-center uk-align-right@m uk-margin-remove-adjacent uk-text-center">
										<div  uk-grid style=" padding-left: 0;">
											<div class="uk-width-1-2 uk-align-center" uk-grid style="padding:0;margin:0"> 
												<div class="uk-width-1-1" style="padding-left:0">
													<p class="pad-marg-0 font-primary movil-text">Tel.(33)36354402</p>
												</div>
												<div class="btn_login font-primary"> '.$loginButton.' </div>
											</div>
											<div class="uk-width-1-2 uk-text-right" uk-grid style="padding-left: 0;margin-left:0;margin-top: 0"> 
												<div class="uk-width-1-1 pad-marg-0" style="padding-left:0;padding-left:0">
													<p class="pad-marg-0 font-primary movil-text">Tel.(33)36354402</p>
												</div>
												<div class="uk-width-1-1 pad-marg-0">
													<a href="'.$socialWhats.'" class="border-cero"  target="_blank" 
													style="margin:0;padding-left:0;margin-bottom:0;padding-bottom:0;">
														<span  uk-icon="icon: whatsapp; ratio: 1" 
														class="pad-marg-0 font-primary"></span>
													</a>
													<a href="'.$socialFace.'" class="border-cero"  target="_blank" 
													style="margin:0;padding-left:0;margin-bottom:0;padding-bottom:0;">
														<span uk-icon="icon: facebook; ratio: 1" 
														class="pad-marg-0 font-primary"></span>
													</a>
													<a href="'.$socialInst.'" class="border-cero"  target="_blank" 
													style="margin:0;padding-left:0;margin-bottom:0;padding-bottom:0;">
														<span uk-icon="icon: instagram; ratio:1" 
														class="pad-marg-0 font-primary"></span>
													</a>
												</div>
											</div>
										</div>

										<!--'.$loginButton.' -->
									</div>
								</div>
								<div class="uk-width-expand@s uk-width-1-3@m for_movil"> 
									<div class="uk-align-center uk-margin-remove-adjacent" style="padding:0 30px; margin-top:30px">
										<div>
										    <div class="uk-width-1-1 uk-search uk-search-default uk-border-pill" style="border:solid 2px #e07134;">
										        <!--span class="uk-search-icon-flip" style="color:#e07134;" uk-search-icon></span-->
										        <a href="" class="uk-search-icon-flip" uk-search-icon style="color:#e07134"></a>
										        <input class="uk-search-input " type="search" placeholder="¿Qué buscabas?">
										    </div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</header>

			'.$mensajes.'

			<!-- Menú móviles -->
			<div id="menu-movil" uk-offcanvas="mode: push;overlay: true" style="z-index:999">
				<div class="uk-offcanvas-bar uk-flex uk-flex-column">
					<div class="uk-width-1-1" >
						<div class="uk-grid-small" uk-grid>
							<div class="uk-width-1-1@s uk-width-1-6@m"> </div>
							<div class="uk-width-1-1@s uk-width-1-4@m"> 
								<h3><button class="uk-offcanvas-close" type="button" uk-close></button>MENÚ</h3> 
							</div>
							<div class="uk-width-expand">
								
							</div>
						</div>
					</div>
					
					<div class="uk-width-1-1" >
						<div class="uk-grid-small" uk-grid>
							<div class="uk-width-1-1@s uk-width-1-6@m"> </div>
							<div class="uk-width-expand">
								<ul class="uk-nav uk-nav-primary uk-nav-parent-icon uk-nav-center uk-margin-auto-vertical" uk-nav>
									'.$menuMovil.'
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>';

/* %%%%%%%%%%%%%%%%%%%% FOOTER                 */
	$whatsIconClass=(isset($_SESSION['whatsappHiden']))?'':'uk-hidden';
	$stickerClass=($carroTotalProds==0 OR $identificador==500 OR $identificador==501 OR $identificador==502)?'uk-hidden':'';
	$footer = '
		
			<div class="uk-width-1-1" style="background:#d9d9d9">
				<div class="uk-container padding-v-50">
					<div class="uk-grid">
						<div class="uk-width-1-1@s uk-width-1-5@m uk-text-center padding-h-50">
							Logo
						</div>
						<div class="uk-width-1-1@s uk-width-1-5@m uk-text-center padding-h-50">
							<div class="oswald title_footer">NAVEGAGION</div>
							<ul class="menu_footer padding-left-0">
								<li>
									<a href="">INICIO</a>
								</li>
								<li>
									<a href="">PRODUCTOS</a>
								</li>
								<li>
									<a href="">PROMOCIONES</a>
								</li>
								<li>
									<a href="">NOSOTROS</a>
								</li>
								<li>
									<a href="">CONTACTO</a>
								</li>
							</ul>
						</div>
						<div class="uk-width-1-1@s uk-width-1-5@m uk-text-center padding-h-50">
							<div class="oswald title_footer">CATEGORIAS</div>
							<ul class="menu_footer  padding-left-0">
								<li>
									<a href="">AVICOLA</a>
								</li>
								<li>
									<a href="">PORCICOLA</a>
								</li>
								<li>
									<a href="">HERRAJES</a>
								</li>
								<li>
									<a href="">EQUIPOS DE CALEFACCIÓN</a>
								</li>
								<li>
									<a href="">EQUIPOS DE CLIMATIZACION</a>
								</li>
							</ul>
						</div>
						<div class="uk-width-1-1@s uk-width-1-5@m uk-text-center padding-h-50">
							<div class="oswald title_footer">CONTACTO</div>
							<ul class="menu_footer padding-left-0">
								<li>
									<a href="">TEL.(33) 3635 4402</a>
								</li>
								<li>
									<a href="">TEL.(33) 3659 0244</a>
								</li>
								<li>
									<a href="'.$socialWhats.'" class="border-cero"  target="_blank">
										<span  uk-icon="icon: whatsapp; ratio: 1" 
										class="pad-marg-0 font-secundario"></span>
									</a>
								
									<a href="'.$socialFace.'" class="border-cero"  target="_blank">
										<span uk-icon="icon: facebook; ratio: 1" 
										class="pad-marg-0 font-secundario"></span>
									</a>
								
									<a href="'.$socialInst.'" class="border-cero"  target="_blank">
										<span uk-icon="icon: instagram; ratio:1" 
										class="pad-marg-0 font-secundario"></span>
									</a>
								</li>
							</ul>
						</div>
						<div class="uk-width-1-1@s uk-width-1-5@m uk-text-center padding-h-50">
							<div class="oswald title_footer">DIRECCIÓN</div>
							<p style="color:#7c330a; font-size:14px;">
								Calle San Ángel No. 538,<br>
								Col. Valle de la Misericordia.<br>
								Tlaquepaque, Jalisco. Mex.
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="bg-footer" style="z-index: 0;">
				<div class="uk-container uk-position-relative">
					<div class="uk-width-1-1 uk-text-center">
						<div class="padding-v-10 font-secundario">
							'.date('Y').' todos los derechos reservados Diseño por <a href="https://wozial.com/" target="_blank" class="font-secundario"> <b>Wozial Marketing Lovers</b></a>
						</div>
					</div>
				</div>
			</div>
		

		<div id="cotizacion-fixed" class="uk-position-top uk-height-viewport '.$stickerClass.'">
			<div>
				<a href="'.$rutaPedido.'"><img src="img/design/checkout.png"></a>
			</div>
		</div>

		'.$loginModal.'

		<!--div id="whatsapp-plugin" class="uk-hidden">
			<div id="whats-head" class="uk-position-relative">
				<div uk-grid class="uk-grid-small uk-grid-match">
					<div>
						<div class="uk-flex uk-flex-center uk-flex-middle">
							<img class="uk-border-circle padding-10" src="img/design/logo-og.jpg" style="width:70px;">
						</div>
					</div>
					<div>
						<div class="uk-flex uk-flex-center uk-flex-middle color-blanco">
							<div>
								<span class="text-sm">'.$Brand.'</span><br>
								<span class="text-6 uk-text-light">Atención en línea vía chat</span>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-position-right color-blanco text-sm">
					<span class="pointer padding-10" id="whats-close">x</spam>
				</div>
			</div>
			<div id="whats-body-1" class="uk-flex uk-flex-middle">
				<div class="bg-white uk-border-rounded padding-h-10" style="margin-left:20px;">
					<img src="img/design/loading.gif" style="height:40px;">
				</div>
			</div>
			<div id="whats-body-2" class="uk-hidden">
				<span class="uk-text-bold uk-text-muted">'.$Brand.'</span><br>
				Hola 👋<br>
				¿Cómo puedo ayudarte?
			</div>
			<div id="whats-footer" class="uk-flex uk-flex-center uk-flex-middle">
				<a href="'.$socialWhats.'" target="_blank" class="uk-button uk-button-small" id="button-whats"><i class="fab fa-whatsapp fa-lg"></i> <span style="font-weight:400;">Comenzar chat</span></a>
			</div>
		</div-->
		

   	</div>';
/* %%%%%%%%%%%%%%%%%%%% HEAD GENERAL                */
	$headGNRL='
		<html lang="'.$languaje.'">
		<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">

			<meta charset="utf-8">
			<title>'.$title.'</title>
			<meta name="description" content="'.$description.'" />
			<meta property="fb:app_id" content="'.$appID.'" />
			<link rel="image_src" href="'.$ruta.$logoOg.'" />

			<meta property="og:type" content="website" />
			<meta property="og:title" content="'.$title.'" />
			<meta property="og:description" content="'.$description.'" />
			<meta property="og:url" content="'.$rutaEstaPagina.'" />
			<meta property="og:image" content="'.$ruta.$logoOg.'" />

			<meta itemprop="name" content="'.$title.'" />
			<meta itemprop="description" content="'.$description.'" />
			<meta itemprop="url" content="'.$rutaEstaPagina.'" />
			<meta itemprop="thumbnailUrl" content="'.$ruta.$logoOg.'" />
			<meta itemprop="image" content="'.$ruta.$logoOg.'" />

			<meta name="twitter:title" content="'.$title.'" />
			<meta name="twitter:description" content="'.$description.'" />
			<meta name="twitter:url" content="'.$rutaEstaPagina.'" />
			<meta name="twitter:image" content="'.$ruta.$logoOg.'" />
			<meta name="twitter:card" content="summary" />

			<meta name="viewport"       content="width=device-width, initial-scale=1">

			<link rel="icon"            href="'.$ruta.'img/design/favicon.ico" type="image/x-icon">
			<link rel="shortcut icon"   href="img/design/favicon.ico" type="image/x-icon">
			<link rel="stylesheet"      href="https://cdnjs.cloudflare.com/ajax/libs/uikit/'.$uikitVersion.'/css/uikit.min.css" />
			<link rel="stylesheet/less" href="css/general.less" >
			<link rel="stylesheet"      href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900&display=swap" >
			<link rel="stylesheet"      href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700&display=swap" >
			<link rel="stylesheet"      href="https://use.fontawesome.com/releases/v5.0.6/css/all.css">


			<!-- jQuery is required -->
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

			<!-- UIkit JS -->
			<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/'.$uikitVersion.'/js/uikit.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/'.$uikitVersion.'/js/uikit-icons.min.js"></script>

			<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.9.0/less.min.js" ></script>			
		</head>';


/* %%%%%%%%%%%%%%%%%%%% SCRIPTS                */
	$scriptGNRL='
		<script src="js/general.js"></script>

		<script>
			$(".cantidad").keyup(function() {
				var inventario = $(this).attr("data-inventario");
				var cantidad = $(this).val();
				inventario=1*inventario;
				cantidad=1*cantidad;
				if(inventario<=cantidad){
					$(this).val(inventario);
				}
				console.log(inventario+" - "+cantidad);
			})
			$(".cantidad").focusout(function() {
				var inventario = $(this).attr("data-inventario");
				var cantidad = $(this).val();
				inventario=1*inventario;
				cantidad=1*cantidad;
				if(inventario<=cantidad){
					//console.log(inventario*2+" - "+cantidad);
					$(this).val(inventario);
				}
			})

			// Agregar al carro
			$(".buybutton").click(function(){
				var id=$(this).data("id");
				var cantidad=$("#"+id).val();

				$.ajax({
					method: "POST",
					url: "addtocart",
					data: { 
						id: id,
						cantidad: cantidad,
						addtocart: 1
					}
				})
				.done(function( msg ) {
					datos = JSON.parse(msg);
					UIkit.notification.closeAll();
					UIkit.notification(datos.msg);
					$("#cartcount").html(datos.count);
					$("#cotizacion-fixed").removeClass("uk-hidden");
				});
			})
		</script>
		';



	// Script login Facebook
	$scriptGNRL.=(!isset($_SESSION['uid']) AND $dominio != 'localhost' AND isset($facebookLogin))?'
		<script>
			// Esta es la llamada a facebook FB.getLoginStatus()
			function statusChangeCallback(response) {
				if (response.status === "connected") {
					procesarLogin();
				} else {
					console.log("No se pudo identificar");
				}
			}

			// Verificar el estatus del login
			function checkLoginState() {
				FB.getLoginStatus(function(response) {
					statusChangeCallback(response);
				});
			}

			// Definir características de nuestra app
			window.fbAsyncInit = function() {
				FB.init({
					appId      : "'.$appID.'",
					xfbml      : true,
					version    : "v3.2"
				});
				FB.AppEvents.logPageView();
			};

			// Ejecutar el script
			(function(d, s, id){
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/es_LA/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, \'script\', \'facebook-jssdk\'));
			
			// Procesar Login
			function procesarLogin() {
				FB.api(\'/me?fields=id,name,email\', function(response) {
					console.log(response);
					$.ajax({
						method: "POST",
						url: "includes/acciones.php",
						data: { 
							facebooklogin: 1,
							nombre: response.name,
							email: response.email,
							id: response.id
						}
					})
					.done(function( response ) {
						console.log( response );
						datos = JSON.parse( response );
						UIkit.notification.closeAll();
						UIkit.notification(datos.msj);
						if(datos.estatus==0){
							location.reload();
						}
					});
				});
			}
		</script>

		':'';


// Reportar actividad
	$scriptGNRL.=(!isset($_SESSION['uid']))?'':'
		<script>
			var w;
			function startWorker() {
			  if(typeof(Worker) !== "undefined") {
			    if(typeof(w) == "undefined") {
			      w = new Worker("js/activityClientFront.js");
			    }
			    w.onmessage = function(event) {
					//console.log(event.data);
			    };
			  } else {
			    document.getElementById("result").innerHTML = "Por favor, utiliza un navegador moderno";
			  }
			}
			startWorker();
		</script>
		';

		

/* %%%%%%%%%%%%%%%%%%%% BUSQUEDA               */
	$scriptGNRL.='
		<script>
			$(document).ready(function(){
				$(".search").keyup(function(e){
					if(e.which==13){
						var consulta=$(this).val();
						var l = consulta.length;
						if(l>2){
							window.location = ("'.$ruta.'"+consulta+"_gdl");
						}else{
							UIkit.notification.closeAll();
							UIkit.notification("<div class=\'bg-danger color-blanco\'>Se requiren al menos 3 caracteres</div>");
						}
					}
				});
				$(".search-button").click(function(){
					var consulta=$(".search-bar-input").val();
					var l = consulta.length;
					if(l>2){
						window.location = ("'.$ruta.'"+consulta+"_gdl");
					}else{
						UIkit.notification.closeAll();
						UIkit.notification("<div class=\'bg-danger color-blanco\'>Se requiren al menos 3 caracteres</div>");
					}
				});
			});
		</script>';

/* %%%%%%%%%%%%%%%%%%%% WHATSAPP PLUGIN               */
	$scriptGNRL.=(isset($_SESSION['whatsappHiden']))?'':'
		<script>
			setTimeout(function(){
				$("#whatsapp-plugin").addClass("uk-animation-slide-bottom-small");
				$("#whatsapp-plugin").removeClass("uk-hidden");
			},1000);
			setTimeout(function(){
				$("#whats-body-1").addClass("uk-hidden");
				$("#whats-body-2").removeClass("uk-hidden");
			},6000);
		</script>
			';

	$scriptGNRL.='
		<script>
			$("#whats-close").click(function(){
				$("#whatsapp-plugin").addClass("uk-hidden");
				$("#whats-show").removeClass("uk-hidden");
				$.ajax({
					method: "POST",
					url: "includes/acciones.php",
					data: { 
						whatsappHiden: 1
					}
				})
				.done(function( msg ) {
					console.log(msg);
				});
			});
			$("#whats-show").click(function(){
				$("#whatsapp-plugin").removeClass("uk-hidden");
				$("#whats-show").addClass("uk-hidden");
				$("#whats-body-1").addClass("uk-hidden");
				$("#whats-body-2").removeClass("uk-hidden");
				$.ajax({
					method: "POST",
					url: "includes/acciones.php",
					data: { 
						whatsappShow: 1
					}
				})
				.done(function( msg ) {
					console.log(msg);
				});
			});
		</script>';



