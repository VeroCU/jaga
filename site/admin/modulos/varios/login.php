<?=$head?>

<style>
  body{ min-height: 100vh; }
  .bg1{ background: url(../img/contenido/wozial/wozial1.jpg); background-size: cover; }
  .bg2{ background: url(../img/contenido/wozial/wozial2.jpg); background-size: cover; }
  .bg3{ background: url(../img/contenido/wozial/wozial3.jpg); background-size: cover; }
  .bg4{ background: url(../img/contenido/wozial/wozial4.jpg); background-size: cover; }
  .bg5{ background: url(../img/contenido/wozial/wozial5.jpg); background-size: cover; }
  .bg6{ background: url(../img/contenido/wozial/wozial6.jpg); background-size: cover; }
  .bg7{ background: url(../img/contenido/wozial/wozial7.jpg); background-size: cover; }
  .bg8{ background: url(../img/contenido/wozial/wozial8.jpg); background-size: cover; }
</style>


<div class="uk-flex uk-flex-center uk-flex-middle" style="min-height: 100vh;">
  <div class="uk-card uk-card-body uk-border-rounded" style="width:300px;background-color: rgba(255,255,255,.9)">

    <div class="uk-text-center">
      <img src="../img/design/logo-wozial.png" class="margin-bottom-10" style="max-height: 100px;">
    </div>

    <form action="index.php" method="post">
      
      <div class="uk-inline">
        <span class="uk-form-icon uk-form-icon-flip" href="" uk-icon="icon: user"></span>
        <input name="user" id="user" type="text" class="uk-input uk-margin uk-width-1-1 uk-form-large" autofocus>
      </div>
      
      <div class="uk-inline">
        <span class="uk-form-icon uk-form-icon-flip" href="" uk-icon="icon: lock"></span>
        <input name="pass" id="pass" type="password" class="pass uk-input uk-margin uk-width-1-1 uk-form-large" placeholder="Contraseña">
      </div>

      <span class="password-revelar uk-margin">Revelar contraseña</span>
      <span class="password-ocultar uk-hidden uk-margin">Ocultar contraseña</span>
      
      <button class="uk-width-1-1 uk-margin uk-button uk-button-primary uk-button-large">Entrar</button>
    
    </form>

  </div>
</div>

<?=$jquery?>

    function fotos(){
      var x = Math.floor((Math.random() * 7) + 1);
      $('body').addClass("bg"+x);
    }
    fotos();

<?=$footer?>
